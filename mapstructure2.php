<div class="map-page">
	<div class="">
		<div class="grid-x align-center">
			<div class="cell medium-6">
				<div class="content">
					<div id="map" style="height:751px;"></div>
				</div>
			</div>
			<div class="cell medium-6 home-map">
				<div class="grid-container">
					<h2><?php _e("Programme Sites" , "unops")  ?></h2>
					<div class="locations-table" >
						<?php 
						$args2 = array(
							'post_type' => 'location',
							'order' => 'ASC',
							'posts_per_page' => -1
						);
						$q2 = new WP_query($args2);
						if($q2->have_posts()) :
							while($q2->have_posts()) : $q2->the_post();
								$theID = get_the_ID();
						?>
							<div class="<?php echo $theID; ?>" id="<?php echo $theID; ?>" data-id="<?php echo $theID; ?>">
								<p style=""><?php the_title(); ?></p>
							</div>
						<?php endwhile;endif; ?>
					</div>
				</div>
				<div class="grid-x align-center" style="margin-top: 60px;">
					<div class="button-holder">
						<a href="https://eu4culture.al/sites/" class="default-button"><?php _e("View All" , "unops")  ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVNxHF0ewVyuoyAz8B0dmNs3PaD6bjaVA&callback=initMap&libraries=&v=weekly"
      async
    ></script>

<script>
	function initMap() { 
    var centerPosition = {lat: 41.1206216, lng: 19.8708004};
    var map = new google.maps.Map(document.getElementById('map'), {
        center: centerPosition,
        zoom : 8,
        mapTypeControl: false
    });
    var prev_infowindow =false; 

<? 

$locationCounter = 0;
$args2 = array(
	'post_type' => 'location',
	'order' => 'ASC',
	'posts_per_page' => -1 
);
$q2 = new WP_query($args2);
if($q2->have_posts()) :
    while($q2->have_posts()) : $q2->the_post();
	$theID = get_the_ID(); ?>
 	 
	<?php $location = get_field('map_location');  ?>
	var position<?php echo $theID; ?> = {lat: <?php echo esc_attr($location['lat']); ?>, lng: <?php echo esc_attr($location['lng']); ?>};  

    var content<?php echo $theID; ?> = "<h6 class='map-title'><?php  the_title(); ?></h6><p><a class='map-link' href='<?php the_permalink(); ?>'><?php esc_html_e( 'Read more', 'text-domain' ); ?><i class='fa fa-arrow-right' aria-hidden='true'></i></a></p>"; 


    var centerInfo<?php echo $theID; ?> = new google.maps.InfoWindow({
    	content: content<?php echo $theID; ?>
    }); 

    
    var marker<?php echo $theID; ?> = new google.maps.Marker({
	        position: position<?php echo $theID; ?>
	    }); 


    // marker<?php echo $theID; ?>.addListener('click', function() {
    	
    // }); 

    marker<?php echo $theID; ?>.addListener("mouseover", () => {
    	$('.locations-table tr').removeClass('active');
		$('tr.<?php echo $theID; ?>').addClass('active');

		if( prev_infowindow ) {
           prev_infowindow.close();
        }

        prev_infowindow = centerInfo<?php echo $theID; ?>;
        centerInfo<?php echo $theID; ?>.open(map, marker<?php echo $theID; ?>); 
	});

    row<?php echo $theID; ?> = document.getElementById('<?php echo $theID; ?>');
    row<?php echo $theID; ?>.addEventListener("click", () => {
    	$('.locations-table tr').removeClass('active');
		$('tr.<?php echo $theID; ?>').addClass('active');

		map.setZoom(10);
    	map.setCenter(marker<?php echo $theID; ?>.getPosition());

		if( prev_infowindow ) {
           prev_infowindow.close();
        }

        prev_infowindow = centerInfo<?php echo $theID; ?>;
        centerInfo<?php echo $theID; ?>.open(map, marker<?php echo $theID; ?>); 
	});



    marker<?php echo $theID; ?>.setMap(map); 


	<? $locationCounter++;endwhile; endif;?>

}

</script>