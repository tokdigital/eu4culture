<?php /* Template Name: Proposals Page */ ?>

<?php get_header(); ?>


<div class="section-press">
	<div class="grid-container">
		<div class="grid-x grid-padding-x align-center ">

			<?php 
			$args = array(
			    'posts_per_page' => -1, 
			    'category_name' => 'proposals',

			);
			$loop = new WP_Query( $args );
			?>
	    	<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
				<div class="cell medium-8 align-center">
					<div class="content-wrapper">
						<div class="default-title text-center"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
							<div class="text-holder">
								<!--  <p>
									<?php the_content(); ?>
								</p> --> 
							</div>
					</div>
				</div>
			<?php endwhile;endif;wp_reset_postdata(); ?>

		</div>
	</div>
</div>


<?php get_footer(); ?>