$(document).foundation();

$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function (event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $("html, body").animate(
          {
            scrollTop: target.offset().top,
          },
          1000,
          function () {
            // Callback after animation
            // Must change focus!
            var $target = $(target);
            $target.focus();
            if ($target.is(":focus")) {
              // Checking if the target was focused
              return false;
            } else {
              $target.attr("tabindex", "-1"); // Adding tabindex for elements not focusable
              $target.focus(); // Set focus again
            }
          }
        );
      }
    }
  });

$(".head-to-head-slider").slick({
  slidesToShow: 1,
  autoPlay: true,
  autoPlaySpeed: 4000,
  arrows: true,
  dots: false,
  prevArrow:
    "<img class='prev' src='wp-content/themes/signalball/img/arrow-left.svg' alt=''>",
  nextArrow:
    "<img class='next' src='wp-content/themes/signalball/img/arrow-right.svg' alt=''>",
});

$(".home-slider-wrapper").slick({
  slidesToShow: 1,
  autoPlay: true,
  autoPlaySpeed: 4000,
  arrows: false,
  dots: true,
});

$(".hero-slider").slick({
  slidesToShow: 1,
  autoplay: true,
  autoplaySpeed: 4000,
  arrows: false,
  dots: true,
  infinte: true,
  fade: true,
});

$(".sports-slider").on(
  "beforeChange",
  function (event, slick, currentSlide, nextSlide) {
    $("[data-goto]").removeClass("active");
    $('[data-goto="' + nextSlide + '"]').addClass("active");
  }
);

$("[data-goto]").on("click", function () {
  gotoNumber = $(this).attr("data-goto");
  $("[data-goto]").removeClass("active");
  $(this).addClass("active");
  $(".sports-slider").slick("slickGoTo", gotoNumber);
});

//ssticky header nav ===========================================

window.onscroll = function () {
  myStickyFunction();
};

var headerMenu = document.getElementById("myHeader");
var sticky = headerMenu.offsetTop;

function myStickyFunction() {
  if (window.pageYOffset > sticky) {
    headerMenu.classList.add("sticky");
  } else {
    headerMenu.classList.remove("sticky");
  }
}
//ssticky header nav ===========================================

check = 0;
var lastScrollTop = 0;
window.addEventListener("scroll", function () {
  scrollTop = $(window).scrollTop();

  var fullHeight = $(window).height();
  var windowTop = window.pageYOffset;
  var windowBottom = windowTop + fullHeight;
  var st = window.pageYOffset || document.documentElement.scrollTop;

  if ($(".section-numbers").length) {
    numz = document.getElementById("numbers-holder");
    var numzTop = numz.offsetTop;
    console.log(scrollTop, fullHeight, numzTop);
    if (scrollTop + fullHeight > numzTop + 50) {
      if (check == 0) {
        check = 1;
        $({ countNum: $(".code1").html() }).animate(
          { countNum: 24 },
          {
            duration: 3000,
            easing: "linear",
            step: function () {
              $(".code1").html(Math.floor(this.countNum));
            },
            complete: function () {
              $(".code1").html(this.countNum);
              //alert('finished');
            },
          }
        );
        $({ countNum: $(".code2").html() }).animate(
          { countNum: 8 },
          {
            duration: 3000,
            easing: "linear",
            step: function () {
              $(".code2").html(Math.floor(this.countNum));
            },
            complete: function () {
              $(".code2").html(this.countNum);
              //alert('finished');
            },
          }
        );
        $({ countNum: $(".code3").html() }).animate(
          { countNum: 11 },
          {
            duration: 3000,
            easing: "linear",
            step: function () {
              $(".code3").html(Math.floor(this.countNum));
            },
            complete: function () {
              $(".code3").html(this.countNum);
              //alert('finished');
            },
          }
        );

        $({ countNum: $(".code4").html() }).animate(
          { countNum: 40 },
          {
            duration: 3000,
            easing: "linear",
            step: function () {
              $(".code4").html(Math.floor(this.countNum));
            },
            complete: function () {
              $(".code4").html(this.countNum);
              //alert('finished');
            },
          }
        );

        $({ countNum: $(".code5").html() }).animate(
          { countNum: 54 },
          {
            duration: 3000,
            easing: "linear",
            step: function () {
              $(".code5").html(Math.floor(this.countNum));
            },
            complete: function () {
              $(".code5").html(this.countNum);
              //alert('finished');
            },
          }
        );
        $({ countNum: $(".code6").html() }).animate(
          { countNum: 250 },
          {
            duration: 3000,
            easing: "linear",
            step: function () {
              $(".code6").html(Math.floor(this.countNum));
            },
            complete: function () {
              $(".code6").html(this.countNum);
              //alert('finished');
            },
          }
        );
        $({ countNum: $(".code7").html() }).animate(
          { countNum: 3 },
          {
            duration: 3000,
            easing: "linear",
            step: function () {
              $(".code7").html(Math.floor(this.countNum));
            },
            complete: function () {
              $(".code7").html(this.countNum);
              //alert('finished');
            },
          }
        );
      }
    }
  }
});

$(".show-canvas").on("click", function () {
  $(".canvas-container").addClass("open");
});
$(".close-canvas , .off-canvas-menu li a").on("click", function () {
  $(".canvas-container").removeClass("open");
});

// $(window).load(function () {
//     $(".trigger_popup_fricc").click(function(){
//        $('.hover_bkgr_fricc').show();
//     });
//     $('.hover_bkgr_fricc').click(function(){
//         $('.hover_bkgr_fricc').hide();
//     });
//     $('.popupCloseButton').click(function(){
//         $('.hover_bkgr_fricc').hide();
//     });
// });

// function myLatestOpportunitiesFunction() {
//   var x = document.getElementById("myDIV");
//   if (x.style.display === "none") {
//     x.style.display = "block";
//   } else {
//     x.style.display = "none";
//   }
// }

$("#close-notification").on("click", function () {
  $("#notification").hide();
});

var swiperGallery = new Swiper("#swiper-gallery", {
  // direction: 'vertical',
  slidesPerView: 1,
  effect: "fade",
  fadeEffect: {
    crossFade: true,
  },
  loop: true,
  scrollbar: {
    el: ".swiper-scrollbar",
    draggable: true,
    dragSize: 100,
    cache: false,
  },
  autoplay: {
    delay: 5000,
  },
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
});
