<?php /* Template Name: Tenders Page */ ?>
<?php get_header(); ?>


<h1 class="page-title"><?php the_title(); ?></h1>

<section class="section-tenders">
	<div class="grid-container">
		<div class="grid-x grid-padding-x align-center"> 
			<div class="cell medium-8">
				<div class="tender-holder">
					<div class="grid-x  align-center">
						<?php 
						 $args = array(
						 	'post_type' => 'tender',
					        'posts_per_page' => -1,
					        'order'         => 'DESC',
					        );
					    $loop = new WP_Query( $args );
					     ?>
						<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
						<div class="post-item" data-equalizer-watch>
							<h4><?php the_title(); ?></h4>
							<p><?php the_content(); ?></p>
							<a href="<?php the_field('linkk') ?>" class="button"><?php _e("Read More" , "unops")  ?></a>
							<!-- <a href="<?php the_permalink(); ?>" class="button">Read More </a> -->
						</div>
						<?php endwhile;endif;wp_reset_postdata();  ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?> 