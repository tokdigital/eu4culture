<?php /* Template Name: Contact Page */ ?>

<?php get_header(); ?>

<h1 class="page-title"><?php the_title(); ?></h1>

<section class="section-contact">
	<div class="grid-container">
		<div class="grid-x">
			<?php if(ICL_LANGUAGE_CODE=='sq'){ ?>
				<div class="cell medium-6">
					<?php echo do_shortcode('[ninja_form id=2]');?>
				</div>
			<?php } else { ?>
				<div class="cell medium-6">
					<?php echo do_shortcode('[ninja_form id=1]');?>
				</div>
			<?php } ?> 
			<div class="cell medium-6">
				<div class="content-social">
					<div class="default-title"><?php _e("Social Media" , "unops")  ?></div>
					<div class="socials">
						<a href="https://www.facebook.com/eu4culturealbania/?amp%3B__tn__=%3C%3C%2CP-R" target="_blank">
							<img src="<?php bloginfo('template_url') ?>/img/fb.svg" alt="">
						</a>
						<a href="https://www.youtube.com/channel/UCofhel3dP1g7SqhrC_ONl2w" target="_blank">
							<img src="<?php bloginfo('template_url') ?>/img/yt.svg" alt="">
						</a>
						<a href="https://twitter.com/eu4culture" target="_blank">
							<img src="<?php bloginfo('template_url') ?>/img/tw.svg" alt="">
						</a>
						<a href="https://www.instagram.com/eu4culturealbania/?utm_medium=copy_link&fbclid=IwAR3pGQxt92gQl3cKF5CG7y4pwuEVaP7MaE-ShyX92eZ8ZuU35lReC1Y4IX8" target="_blank">
							<img src="<?php bloginfo('template_url') ?>/img/ig.svg" alt="">
						</a>
					</div>
					<div class="address">
						<div class="default-title connect"><?php _e("Address" , "unops")  ?>:</div>
						<p><?php _e("UN House Skanderbeg Street" , "unops")  ?>,<br>
						<?php _e("Gurten Building 3rd Floor" , "unops")  ?><br>
						<?php _e("Tirana, Albania" , "unops")  ?></p>
					</div>
						
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>