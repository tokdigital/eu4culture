<?php

add_theme_support( 'menus' );
add_theme_support( 'widgets' );
add_theme_support( 'post-thumbnails' ); 
 

 function tok_scripts() {

    wp_enqueue_style( 'font-awesome', 'https://use.fontawesome.com/releases/v5.7.2/css/all.css', '', '', false);
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Cinzel:400,700,900&display=swap', '', '', false);
    wp_enqueue_style( 'wow-stylesheet', get_stylesheet_directory_uri() . '/scss/animate.css?v' . time(), '', '', false);
    wp_enqueue_style( 'slick-stylesheet', get_stylesheet_directory_uri() . '/node_modules/slick-carousel/slick/slick.css?v' . time(), '', '', false);
    wp_enqueue_style( 'main-stylesheet', get_stylesheet_directory_uri() . '/css/app.css?v' . time(), '', '', false);
    wp_enqueue_style( 'slick', get_stylesheet_directory_uri(). '/node_modules/slick-carousel/slick/slick.css', '', '', false);
    wp_enqueue_style( 'swiperjs', 'https://unpkg.com/swiper@7/swiper-bundle.min.css', '', '', false);
    wp_enqueue_style( 'lightbox2-stylesheet', get_stylesheet_directory_uri() . '/node_modules/lightbox2/dist/css/lightbox.min.css', '', '', false);

    wp_deregister_script('jquery');

    wp_enqueue_script( 'jquery', get_stylesheet_directory_uri(). '/node_modules/jquery/dist/jquery.min.js', array(), '', false);

    wp_enqueue_script( 'what-input', get_stylesheet_directory_uri(). '/node_modules/what-input/dist/what-input.min.js', array(), '', true);
    wp_enqueue_script( 'foundation', get_stylesheet_directory_uri(). '/node_modules/foundation-sites/dist/js/foundation.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'aos', get_stylesheet_directory_uri(). '/node_modules/aos/dist/aos.js', array(), '', true);
    wp_enqueue_script( 'slick', get_stylesheet_directory_uri() . '/node_modules/slick-carousel/slick/slick.min.js', '', '', false);
    wp_enqueue_script( 'swiperjs', 'https://unpkg.com/swiper@7/swiper-bundle.min.js', array('jquery'), '', true);
    wp_enqueue_script( 'slick-carousel', get_stylesheet_directory_uri(). '/node_modules/slick-carousel/slick/slick.min.js', array('jquery'), '', true);
    wp_enqueue_script( 'lightbox2', get_stylesheet_directory_uri(). '/node_modules/lightbox2/dist/js/lightbox.min.js', array('jquery'), '', true);
    wp_enqueue_script( 'wow-scripts', get_stylesheet_directory_uri(). '/js/wow.min.js?v' . time(), array(), '', true );
    wp_enqueue_script( 'circles-scripts', get_stylesheet_directory_uri(). '/js/circles.min.js?v' . time(), array(), '', true );
    // wp_enqueue_script( 'jquery', get_stylesheet_directory_uri(). '/node_modules/countup/countUp.js', array(), '', false);

    wp_enqueue_script( 'tok-scripts', get_stylesheet_directory_uri(). '/js/app.js?v' . time(), array(), '', true );

}


add_action( 'wp_enqueue_scripts', 'tok_scripts' );


function my_acf_google_map_api( $api ){
    
    $api['key'] = 'AIzaSyDcD4eoXwTX32GvWmjeqRVWUDE_uGUVPWk';
    
    return $api;
    
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

// function custom_posttype() {
// 	register_post_type( 'testimonial', 
// 		array(
// 			'labels' => array(
// 				'name' => __( 'Testimonials' ),
// 				'singular_name' => __( 'testimonial' )
// 			),
// 			'public' => true,
// 			'has_archive' => true,
// 			'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
// 			'taxonomies'  => array( 'testimonial-category' )
// 		)
// 	);

//     register_post_type( 'gallery', 
//         array(
//             'labels' => array(
//                 'name' => __( 'Galleries' ),
//                 'singular_name' => __( 'gallery' )
//             ),
//             'public' => true,
//             'has_archive' => true,
//             'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
//             'taxonomies'  => array( 'gallery-category' )
//         )
//     );
// }
// add_action( 'init', 'custom_posttype' );


// function custom_taxonomies() {
 
//     register_taxonomy(
//         'gallery-category',
//         'gallery',
//         array(
//             'label' => __( 'Kategorite' ),
//             'rewrite' => array( 'slug' => 'gallery-category' ),
//             'hierarchical' => true,
//         )
//     );
// }
// add_action( 'init', 'custom_taxonomies' );


function location_posttype() {
    register_post_type( 'location', 
        array(
            'labels' => array(
                'name' => __( 'Locations' ),
                'singular_name' => __( 'location' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
            'taxonomies'  => array( 'location-category' )
        )
    );

    register_post_type( 'location', 
        array(
            'labels' => array(
                'name' => __( 'Locations' ),
                'singular_name' => __( 'location' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
            'taxonomies'  => array( 'location-category' )
        )
    );
}
add_action( 'init', 'location_posttype' );



function media_posttype() {
    register_post_type( 'media', 
        array(
            'labels' => array(
                'name' => __( 'Medias' ),
                'singular_name' => __( 'media' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
            'taxonomies'  => array( 'media-category' )
        )
    );

    register_post_type( 'media', 
        array(
            'labels' => array(
                'name' => __( 'Medias' ),
                'singular_name' => __( 'media' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
            'taxonomies'  => array( 'media-category' )
        )
    );
}
add_action( 'init', 'media_posttype' );



function gallery_posttype() {
    register_post_type( 'gallery', 
        array(
            'labels' => array(
                'name' => __( 'Galleries' ),
                'singular_name' => __( 'gallery' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
            'taxonomies'  => array( 'gallery-category' )
        )
    );

    register_post_type( 'gallery', 
        array(
            'labels' => array(
                'name' => __( 'Galleries' ),
                'singular_name' => __( 'gallery' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
            'taxonomies'  => array( 'gallery-category' )
        )
    );
}
add_action( 'init', 'gallery_posttype' );



function video_posttype() {
    register_post_type( 'video', 
        array(
            'labels' => array(
                'name' => __( 'Videos' ),
                'singular_name' => __( 'video' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
            'taxonomies'  => array( 'video-category' )
        )
    );

    register_post_type( 'video', 
        array(
            'labels' => array(
                'name' => __( 'Videos' ),
                'singular_name' => __( 'video' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
            'taxonomies'  => array( 'video-category' )
        )
    );
}
add_action( 'init', 'video_posttype' );

function tender_posttype() {
    register_post_type( 'tender', 
        array(
            'labels' => array(
                'name' => __( 'Tenders' ),
                'singular_name' => __( 'tender' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
            'taxonomies'  => array( 'tender-category' )
        )
    );

    register_post_type( 'tender', 
        array(
            'labels' => array(
                'name' => __( 'Tenders' ),
                'singular_name' => __( 'tender' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
            'taxonomies'  => array( 'tender-category' )
        )
    );
}
add_action( 'init', 'tender_posttype' );


function jobs_posttype() {
    register_post_type( 'jobs', 
        array(
            'labels' => array(
                'name' => __( 'Jobs' ),
                'singular_name' => __( 'job' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
            'taxonomies'  => array( 'jobs-category' )
        )
    );

    register_post_type( 'jobs', 
        array(
            'labels' => array(
                'name' => __( 'Jobs' ),
                'singular_name' => __( 'job' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
            'taxonomies'  => array( 'jobs-category' )
        )
    );
}
add_action( 'init', 'jobs_posttype' );






function site_posttype() { 

    register_post_type( 'site', 
        array(
            'labels' => array(
                'name' => __( 'Sites' ),
                'singular_name' => __( 'site' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
            'taxonomies'  => array( 'site-category' )
        )
    );
}
add_action( 'init', 'site_posttype' );




?>