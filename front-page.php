<?php get_header(); ?>



<div class="notification" id="notification">
	<div class="notification-inner">
		<button class="close-notification" id="close-notification">X</button>
		<div class="notification-widget separation-line">

			<?php
			if (have_rows('popup_home')) :
				while (have_rows('popup_home')) : the_row(); ?>
					<h4 id="popup-title"><?php the_sub_field('title'); ?></h4>
					<?php if (get_sub_field('description')) : ?>
						<h5 style="font-size: 12px; color: #223167;"><?php the_sub_field('description'); ?></h5>
					<?php endif; ?>
					<?php if (get_sub_field('button')) : ?>
						<a href="<?php the_sub_field('button'); ?>" class="default-button2" id="popup-button"><?php _e("Read more", "unops")  ?></a>
					<?php endif; ?>
			<?php endwhile;
			else : endif; ?>

		</div>
		<!-- <?php if (ICL_LANGUAGE_CODE == 'en') { ?>
				<div>
					<a href="https://eu4culture.al/wp-content/uploads/2022/07/List-of-applicants-and-projects-recommended-for-funding-CfP-02-2022.pdf">
						<h5 style="font-size: 1.1rem; color: #223167;">List of winners of the Re-Craft the Past: Build up the Future CfP</h5>
					</a>
				</div>
			<?php } else { ?>
				<div>
					<a href="https://eu4culture.al/wp-content/uploads/2022/07/List-of-applicants-and-projects-recommended-for-funding-CfP-02-2022.pdf">
						<h5 style="font-size: 1.1rem; color: #223167;">Lista e fituesve të Thirrjes Re-Craft the Past: Build up the Future </h5>
					</a>
				</div>
		<?php } ?> -->
	</div>

</div>



<section class="section-hero">
	<div class="hero-slider">
		<?php
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => 3,
			'order' => 'DESC'
		);
		$loop = new WP_Query($args);
		?>
		<?php if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
				<div>
					<div class="single-slide-wrapper">
						<div class="slider-image">
							<?php the_post_thumbnail(); ?>
						</div>
						<div class="slider-content">
							<div class="grid-container">
								<div class="grid-x">
									<div class="cell medium-6">
										<div class="default-title white"><?php the_title(); ?></div>
										<div class="holder">
											<div class="date">
												<div class="icon show-for-medium"><img src="<?php bloginfo('template_url') ?>/img/calendar-white.svg" alt=""></div>
												<p class="white"><?php $post_date = get_the_date('F j, Y');
																	echo $post_date; ?></p>
											</div>
											<a href="<?php the_permalink(); ?>" class="default-button transparent">
												<?php _e("Read More", "unops")  ?>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

		<?php endwhile;
		endif;
		wp_reset_postdata(); ?>
	</div>
</section>


<section class="section-about">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				<div class="img-wrapper">
					<img class="show-for-medium" src="<?php bloginfo('template_url') ?>/img/about-image.jpg" alt="About Unops">
				</div>
			</div>
			<div class="cell medium-6">
				<div class="content-wrapper">
					<div class="default-title"><?php _e("About Us", "unops")  ?></div>
					<div class="text-holder">
						<p><?php _e("EU4Culture project is funded by the European Union (EU) and is implemented by UNOPS in a close partnership with the Ministry of Culture. It is one of the largest cultural heritage programmes designed by the European Union with a total budget of € 40 million. EU4Culture programme extends to the entire territory affected by the earthquake of November 26, 2019. In addition to the restoration, the project will invest in the revitalization of selected cultural heritage sites, including upgrade of services and site conditions. It’s goal is also to increase tourism potential, directly contributing towards Albania's socio-economic recovery. Local entrepreneurship, artisanship and creative creation evolving around selected sites shall be supported through grants, providing a direct boost to local production and social wellbeing.", "unops")  ?></p>
					</div>
					<a class="default-button" href="/about">
						<?php _e("Read More", "unops")  ?>
					</a>
				</div>
				<img class="art show-for-medium" src="<?php bloginfo('template_url') ?>/img/artt.svg" alt="Unops">
			</div>
		</div>
	</div>
</section>


<section class="section-news">
	<div class="grid-container">
		<div class="grid-x grid-padding-x align-center">
			<div class="cell medium-12">
				<div class="content-wrapper">
					<h2 class="default-title text-center"><?php _e("Latest News", "unops")  ?>
				</div>
			</div>
		</div>
		<div class="cell medium-12">
			<div class="news-holder">
				<div class="grid-x grid-padding-x medium-up-4" data-equalizer data-equalize-on="medium">
					<?php
					$args = array(
						'posts_per_page' => 4,
						'category_name' => 'events'
					);
					$loop = new WP_Query($args);
					?>
					<?php if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
							<div class="cell medium-4">
								<div class="single-news" data-equalizer-watch>
									<a href="<?php the_permalink(); ?>" class="featured-image">
										<?php the_post_thumbnail(); ?>
									</a>
									<a href="<?php the_permalink(); ?>" class="small-title">
										<?php the_title(); ?>
									</a>
									<p style="padding: 0 25px;    min-height: 29px;"><?php $post_date = get_the_date('F j, Y');
																						echo $post_date; ?></p>
									<a href="<?php the_permalink(); ?>" class="read-more">
										<?php _e("Read More", "unops")  ?> <img src="<?php bloginfo('template_url') ?>/img/arrow-right-red.svg" alt="">
									</a>
									<div class="decor">
										<img src="<?php bloginfo('template_url') ?>/img/news-decor.svg" alt="">
									</div>
								</div>
							</div>

					<?php endwhile;
					endif;
					wp_reset_postdata();  ?>
				</div>
			</div>
		</div>
		<div class="grid-x">
			<div class="button-holder">
				<a href="/news" class="default-button"><?php _e("View More", "unops")  ?></a>
			</div>
		</div>
	</div>
	</div>
</section>

<div class="section-numbers">
	<div class="grid-container">
		<div class="grid-x grid-padding-x align-center">
			<div class="cell medium-12">
				<div class="content-wrapper">
					<div class="default-title center"><?php _e("EU4CULTURE In Numbers", "unops")  ?></div>
				</div>
			</div>
			<div class="cell medium-7">
				<div class="text-holder text-center">

				</div>
			</div>
			<div class="cell medium-8">
				<div class="image-holder text-center">
					<img src="<?php bloginfo('template_url') ?>/img/now.jpg" alt="">
				</div>
			</div>
			<div class="cell medium-12">
				<div class="numbers-holder" id="numbers-holder">
					<div class="grid-x grid-padding-x">
						<div class="cell medium-4">
							<a href="https://eu4culture.al/sites/">
								<div class="single-number">
									<div class="icon">
										<img src="<?php bloginfo('template_url') ?>/img/numbers/icon1.svg" alt="">
									</div>
									<div class="content">
										<div class="code1 default-title" data="24">0</div>
										<p><?php _e("Sites targeted by the Programme", "unops")  ?></p>
									</div>
								</div>
							</a>
						</div>



						<div class="cell medium-4">

							<div class="single-number">
								<div class="icon">
									<img src="<?php bloginfo('template_url') ?>/img/numbers/icon2.svg" alt="">
								</div>
								<div class="content">
									<span class="code2 default-title" data="8">0</span>
									<p><?php _e("Municipalities targeted by the Program", "unops")  ?></p>
								</div>
							</div>

						</div>
						<div class="cell medium-4">
							<div class="single-number">
								<div class="icon">
									<img src="<?php bloginfo('template_url') ?>/img/numbers/icon3.svg" alt="">
								</div>
								<div class="content">
									<span class="code3 default-title" data="11">0</span>
									<p><?php _e("Ongoing Sites", "unops")  ?></p>
								</div>
							</div>
						</div>
						<div class="cell medium-4">
							<div class="single-number">
								<div class="icon">
									<img src="<?php bloginfo('template_url') ?>/img/numbers/citiz.png" alt="">
								</div>
								<?php if (ICL_LANGUAGE_CODE == 'sq') { ?>
									<div class="content">
										<span class="code4 sq default-title" data="40">0</span>
										<p><?php _e("Total Budget", "unops")  ?></p>
									</div>
								<?php } else { ?>
									<div class="content">
										<span class="code4 default-title" data="1">0</span>
										<p><?php _e("Total Budget", "unops")  ?></p>
									</div>
								<?php } ?>
							</div>
						</div>


						<div class="cell medium-4">
							<a href="https://eu4culture.al/events/">
								<div class="single-number">
									<div class="icon">
										<img src="<?php bloginfo('template_url') ?>/img/numbers/icon5.svg" alt="">
									</div>
									<div class="content">
										<span class="code5 default-title" data="54">0</span>
										<p><?php _e("Consultation meetings", "unops")  ?></p>
									</div>
								</div>
							</a>
						</div>
						<div class="cell medium-4">
							<div class="single-number">
								<div class="icon">
									<img src="<?php bloginfo('template_url') ?>/img/numbers/icon6.svg" alt="">
								</div>
								<div class="content">
									<span class="code6 default-title" data="250">0</span>
									<p><?php _e("Policymakers, academics, media, community representatives participated in Consultation meetings", "unops")  ?></p>
								</div>
							</div>
						</div>
						<div class="cell medium-4">
							<div class="single-number">
								<div class="icon">
									<img src="<?php bloginfo('template_url') ?>/img/numbers/icon3.svg" alt="">
								</div>
								<div class="content">
									<span class="code7 default-title" data="3">0</span>
									<p><?php _e("Restored Sites", "unops")  ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="section-programme">

	<a href="https://eu4culture.al/wp-content/uploads/2022/03/Unops-A4-mars-1-1-1.pdf" target="_blank">
		<img src="https://eu4culture.al/wp-content/uploads/2022/03/Group-258.png" class="programme-img" alt="">
		<img src="https://eu4culture.al/wp-content/uploads/2022/03/bannerpdf-mobile.jpg" class="small-only">
	</a>
</div>

<section class="section-map">
	<?php get_template_part('mapstructure2') ?>
</section>

<div class="section-events">
	<div class="grid-x">
		<div class="cell medium-6">
			<div class="content-events">
				<div class="default-title"><?php _e("Events", "unops")  ?></div>
				<div class="events">
					<?php
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 3,
						'orderby' => 'post_date',
						'oderby' => 'date',
						'order' => 'DESC'
					);
					$loop = new WP_Query($args);
					?>
					<?php if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
							<div class="single-event">
								<div class="date">
									<?php the_post_thumbnail(); ?>
									<span><strong><?php echo get_the_date("M") . "<br>"; ?></strong><?php echo get_the_date("d") . "<br>"; ?></span>

								</div>
								<div class="content">
									<a href="<?php the_permalink(); ?>" class="small-title red">
										<?php the_title(); ?>
									</a>

								</div>
							</div>

					<?php endwhile;
					endif;
					wp_reset_postdata(); ?>
				</div>
				<a href="/events">
					<div class="default-button"><?php _e("View All", "unops")  ?></div>
				</a>
			</div>
		</div>

		<div class="cell medium-6 gallery-wrapper">

			<div class="swiper" id="swiper-gallery">
				<div class="swiper-wrapper">
					<div class=""></div>
					<?php
					$args = array(
						'post_type' => 'site',
						'posts_per_page' => -1,
					);
					$loop = new WP_Query($args);
					?>
					<?php if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
							<div class="cell swiper-slide">
								<h1 class="title" style=""><?php _e("Site Gallery", "unops")  ?></h1>
								<div class="post-item" data-equalizer-watch>
									<a href="<?php the_permalink(); ?>" class="featured-image" target="_blank">
										<?php the_post_thumbnail(); ?>
									</a>

									<h3><a href="<?php the_permalink(); ?>" class="post-title"><?php the_title(); ?></a></h3>
								</div>
							</div>
					<?php endwhile;
					endif;
					wp_reset_postdata();  ?>
				</div>
				<div class="swiper-button-holder">
					<div class="swiper-button-prev"></div>
					<div class="swiper-button-next"></div>
				</div>

			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>