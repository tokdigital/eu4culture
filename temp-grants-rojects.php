<?php /* Template Name: Grants Projects */ ?>

<?php get_header(); ?>

<article>

    <h1 class="page-title"><?php the_title(); ?></h1>

    <div class="grid-container">
        <div class="grid-x align-center">
            <div class="cell medium-10 ">
                <div class="content">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>

</article>

<?php get_footer(); ?>