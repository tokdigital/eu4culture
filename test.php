<?php /* Template Name: test-map */ ?>
<?php get_header(); ?>

<div class="section-events">
	<div class="grid-x">
		<div class="cell medium-6">
			<div class="content-events">
				<div class="default-title"><?php _e("Events" , "unops")  ?></div>
				<div class="events">
					<?php 
					 $args = array(
					 	'post_type' => 'post',
				        'posts_per_page' => 3,
				        'category_name' => 'events'
				        );
				    $loop = new WP_Query( $args );
				     ?>
					<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
					<div class="single-event">
						<div class="date">
							<?php the_post_thumbnail(); ?>
							<span><strong><?php echo get_the_date("M") . "<br>"; ?></strong><?php echo get_the_date("d") . "<br>"; ?></span>
							<!-- <span><strong>25</strong><?php _e("Feb" , "unops")  ?></span> -->
						</div>
						<div class="content">
							<a href="<?php the_permalink(); ?>" class="small-title red">
							<?php the_title(); ?>
						</a>
							<!-- <p><?php the_excerpt(); ?></p> -->
						</div>
					</div> 
					
					<?php endwhile;endif;wp_reset_postdata(); ?>
				</div>
				<a href="/events">
					<div class="default-button"><?php _e("View All" , "unops")  ?></div>
				</a>
			</div>
		</div>



		<div class="cell medium-6 gallery-wrapper">
			
			<div class="swiper" id="swiper-gallery">
				<div class="swiper-wrapper">
					<?php 
					$args = array(
						'post_type' => 'site',
						'posts_per_page' => -1,
						);
					$loop = new WP_Query( $args );
					?>
					<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
						<div class="cell swiper-slide">
						<h1 class="" style="position: absolute; top:30px; left:25px; z-index:199; font-size:49px; font-weight:900; color:#223167;"><?php _e("Site Gallery" , "unops")  ?></h1>
							<div class="post-item" data-equalizer-watch style="max-height: 555px;">
								<a href="<?php the_permalink(); ?>" class="featured-image" target="_blank" >
								<?php the_post_thumbnail(); ?>
								</a>
								
								<h3><a href="<?php the_permalink(); ?>"style=" color: white;
									position: absolute;
									z-index: 10000;
									bottom: 125px;
									left: 25px;
									font-weight: 900;
									font-size: 49px;">
									<?php the_title(); ?></a>
								</h3>
							</div>
						</div>
					<?php endwhile;endif;wp_reset_postdata();  ?>
				</div>
				<div class="swiper-button-holder" style="background-color: #223167;
														height: 80px;
														position: absolute;
														width: 200px;
														bottom: 0px;
														z-index: 999;">
					<div class="swiper-button-prev"></div>
  					<div class="swiper-button-next"></div>
				</div>
				
			</div>
		</div>
	</div>
</div>	
<style>

</style>
<?php get_footer(); ?>