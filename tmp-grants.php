<?php /* Template Name: Grants Page */ ?>

<?php get_header(); ?>

<article>

	<h1 class="page-title"><?php the_title(); ?></h1>

	<div class="grid-container">
		<div class="grid-x align-center">
			<div class="cell medium-7 ">
				<div class="content">
					<?php the_content(); ?>
				</div>
			</div>
			<div class="cell medium-4 medium-offset-1 grants-img">
				<div class="content">

					<?php


					if (have_rows('button2')) :

						while (have_rows('button2')) : the_row(); ?>
							<?php if (get_sub_field('title')) : ?>
								<?php if (ICL_LANGUAGE_CODE == 'en') { ?>
									<a href="https://eu4culture.al/recraft-the-past-build-up-the-future-2/" class="button call-2">
										<?php the_sub_field('title'); ?>
									</a>
								<?php } else { ?>
									<a href="https://eu4culture.al/sq/recraft-the-past-build-up-the-future-2/" class="button call-2">
										<?php the_sub_field('title'); ?>
									</a>
								<?php } ?>
							<?php endif; ?>
							<?php if (get_sub_field('text')) : ?>
								<p><?php the_sub_field('text'); ?></p>
							<?php endif; ?>

					<?php endwhile;
					else : endif;  ?>
					<div style="margin-bottom:150px"><?php the_post_thumbnail(); ?></div>

					<?php if (have_rows('button')) :


						while (have_rows('button')) : the_row(); ?>
							<?php if (get_sub_field('title')) : ?>
								<?php if (ICL_LANGUAGE_CODE == 'en') { ?>
									<a href="https://eu4culture.al/call-for-proposals/" class="button"><?php the_sub_field('title'); ?></a>
								<?php } else { ?>
									<a href="https://eu4culture.al/sq/thirrje-per-propozime/" class="button"><?php the_sub_field('title'); ?></a>
								<?php } ?>
							<?php endif; ?>
							<?php if (get_sub_field('text')) : ?>
								<p><?php the_sub_field('text'); ?></p>
							<?php endif; ?>

					<?php endwhile;
					else : endif;

					?>





					<!-- <?php if (ICL_LANGUAGE_CODE == 'en') { ?>
						<a href="https://eu4culture.al/call-for-proposals/" class="button"><?php _e("Open Call For Proposals", "unops")  ?></a>
					<?php } else { ?>
						<a href="https://eu4culture.al/sq/thirrje-per-propozime/" class="button"><?php _e("Open Call For Proposals", "unops")  ?></a>
					<?php } ?>  -->


				</div>
			</div>
		</div>
	</div>

</article>

<?php get_footer(); ?>