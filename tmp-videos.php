<?php /* Template Name: Videos Page */ ?>
<?php get_header(); ?>

<h1 class="page-title"><?php the_title(); ?></h1>

<!-- <section class="section-videos">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-4">
				<iframe width="400" height="200" src="https://www.youtube.com/embed/2tORnsSjo5A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
				</iframe>
			</div>
			<div class="cell medium-4">
				<iframe width="400" height="200" src="https://www.youtube.com/embed/ohqGg68LD0w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<div class="cell medium-4">
				<iframe width="400" height="200" src="https://www.youtube.com/embed/Bb2V24DenQE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<div class="cell medium-4">
				<iframe width="400" height="200" src="https://www.youtube.com/embed/XYy8iUcHiCk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<div class="cell medium-4">
				<iframe width="400" height="200" src="https://www.youtube.com/embed/2tORnsSjo5A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>	
			
		</div>
	</div>		
</section> -->



<div class="section-videos">
	<div class="grid-container">
		<div class="grid-x grid-padding-x medium-up-4" data-equalizer data-equalize-on="medium">
			<?php 
			 $args = array(
			 	'post_type' => 'video',
		        'posts_per_page' => -1,
		        

		        );
		    $loop = new WP_Query( $args );
		     ?>
			<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
				<div class="cell">
					<div class="post-item" data-equalizer-watch>
						<a href="<?php echo get_the_excerpt(); ?>" class="featured-image" target="_blank">
							<?php the_post_thumbnail(); ?>
						</a>
						<h4><a href="<?php echo get_the_excerpt(); ?>" target="_blank"><?php the_title(); ?></a></h4>
					</div>
				</div>
			<?php endwhile;endif;wp_reset_postdata();  ?>

		</div>
	</div>
</div>




<?php get_footer(); ?>