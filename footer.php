</main>
 
<footer class="footer" id="footer">

	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-4">
				<div class="footer-logo">
					<a href="<?php echo site_url(); ?>"><img src="<?php bloginfo('template_url') ?>/img/logo.svg" alt=""></a>
				</div>
				<div class="logos">
                    <a href="#">
                        <img src="<?php bloginfo('template_url') ?>/img/eu-header.svg" alt="">
                    </a>
                    <a href="#">
                        <img src="<?php bloginfo('template_url') ?>/img/mk-header.png" alt="">
                    </a>
                    <a href="#">
                        <img src="<?php bloginfo('template_url') ?>/img/unops.svg" alt="">
                    </a>
				</div>
			</div>
			<div class="cell medium-5">
				<div class="default-title">
				<?php _e("Short Links" , "unops")  ?>
				</div>
				<div class="footer-menu">
					<?php 
                        wp_nav_menu(
                        array(
                            'menu'=>'Main Menu'
                        )); 
                    ?>
				</div>
			</div>
			<div class="cell medium-3">
				<div class="default-title">
				<?php _e("Get in Touch " , "unops")  ?><br>
				<?php _e("UNOPS Albania" , "unops")  ?>
				</div>
				<div class="touch">
					<div class="single">
						<img src="<?php bloginfo('template_url') ?>/img/location.svg" alt="">
						<span><?php _e("UN House Skanderbeg Street, Gurten Building 3rd Floor | Tirana, Albania" , "unops")  ?></span>
					</div>
					<div class="single">
						<img src="<?php bloginfo('template_url') ?>/img/mail.svg" alt="">
						<span>info@eu4culture.al</span>
					</div>
					<div class="single">
						<a href="https://www.facebook.com/eu4culturealbania/?amp%3B__tn__=%3C%3C%2CP-R" target="_blank"><img src="<?php bloginfo('template_url') ?>/img/facebook.svg" alt=""></a>
						<a href="https://www.youtube.com/channel/UCofhel3dP1g7SqhrC_ONl2w" target="_blank"><img src="<?php bloginfo('template_url') ?>/img/youtube.svg" alt=""></a>
						<a href="https://twitter.com/eu4culture" target="_blank"><img src="<?php bloginfo('template_url') ?>/img/twitter.svg" alt=""></a>
						<a href="https://www.instagram.com/eu4culturealbania/?utm_medium=copy_link&fbclid=IwAR25USZs1a3MRTTAZUoEIvYRbhsN8qjShbnXwG0NxLzDkpNjF2e_9CB_BAI" target="_blank"><img src="<?php bloginfo('template_url') ?>/img/instagram.svg" alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</div> 
</footer> 

<div class="disclaimer text-center medium-text-left">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-2">
				<a href="#" class="eu"><img src="<?php bloginfo('template_url') ?>/img/eu.svg" alt=""></a>
			</div>
			<div class="cell medium-7">
				<p class="dic"><?php _e("This website was created and maintained with the financial support of the European Union. Its
				contents are the sole responsibility of UNOPS and do not necessarily reflect the views of the
				European Union." , "unops")  ?></p>
			</div>
			<div class="cell medium-3">
				<div class="links">
					<a href="#"><?php _e("Policy Privacy" , "unops")  ?></a><span> - </span>
					<a href="#"><?php _e("Help" , "unops")  ?></a>
					
				</div>
			</div>
		</div>

		<div class="copyright text-center">
			Copyright © 2021 Unops All rights reserved. This website is created by Tok Digital Agency, a <a href="https://tok.al" target="_blank">Web development company in Albania</a>.
		</div>

	</div>
</div>


 
    <?php wp_footer() ?>
  </body>
</html>