<?php get_header(); ?> 

<?php if(have_posts()) : while (have_posts()) : the_post(); 
				 ?>

<h1 class="page-title"><?php the_title(); ?></h1>

<div class="single-project">
	<div class="grid-container">
		<div class="grid-x align-center">
			<div class="cell medium-8">
				<div class="project-image">
					<?php the_post_thumbnail(); ?>
				</div>
				<p><?php the_content();?></p>
			</div>
		</div>
	</div>
</div>


<?php endwhile;endif;wp_reset_postdata(); ?>

<?php get_footer(); ?>
