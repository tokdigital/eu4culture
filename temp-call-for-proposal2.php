<?php /* Template Name: Call For Proposal 2 */ ?>

<?php get_header(); ?>


<img src="<?php bloginfo('template_url') ?>/img/banner2.jpg" alt="">


<div class="grid-container">
	<div class="grid-x align-center">
		<div class="cell medium-8">
			<div class="content">
				<?php the_content(); ?>


				<?php if (ICL_LANGUAGE_CODE == 'en') { ?>
					<div class="grid-x grid-padding-x ">
						<div class="cell medium-6 content-margin">
							<p>The applicant must submit the following application forms:</p>
							<div class="wp-container-62054074583ad wp-block-group">
								<div class="wp-block-group__inner-container">
									<ul>
										<li>
											<mark style="background-color: rgba(0, 0, 0, 0);" class="has-inline-color has-vivid-cyan-blue-color"><em><a href="https://eu4culture.al/wp-content/uploads/2022/10/2.-CFP-10-2022-Annex-A-Application-Form.docx" target="_blank" rel="noreferrer noopener"><strong><u>Application Form (Annex A)</u></strong></a></em></mark>
										</li>
										<br>
										<li><a href="https://eu4culture.al/wp-content/uploads/2022/10/3.-CFP-10-2022-Annex-B-Logframe.docx"><em><strong><u>Logical Framework (Annex </u></strong></em><u><em><strong><em><strong>B)</strong></em></strong></em></u></a>
										</li>
										<br>
										<li>
											<mark style="background-color: rgba(0, 0, 0, 0);" class="has-inline-color has-vivid-cyan-blue-color"><em><a href="https://eu4culture.al/wp-content/uploads/2022/10/4.-CFP-10-2022-Annex-C-Application-Budget.xlsx" target="_blank" rel="noreferrer noopener"><strong><u>Application Budget (Annex C) </u></strong></a></em></mark>
										</li>
										<br>
										<li>
											<em><a href="https://eu4culture.al/wp-content/uploads/2022/10/5.-CFP-10-2022-Annex-D-Statement-of-Applicant.docx" target="_blank" rel="noreferrer noopener"><strong><u>Statement of Applicant (Annex D)</u></strong></a></em>
										</li>
										<br>
										<li><strong><em><a href="https://eu4culture.al/wp-content/uploads/2022/10/6.-CFP-10-2022-Annex-E-Statement-of-Partner-Organisation.docx" target="_blank" rel="noreferrer noopener"><u>Statement of Partner organization (Annex E)</u></a></em></strong>
										</li>
										<br>
										<li>
											<strong><em><a href="https://eu4culture.al/wp-content/uploads/2022/10/7.-CFP-10-2022-Annex-F-Application-Checklist.docx" target="_blank" rel="noreferrer noopener"><u>Application Checklist (Annex F)</u></a></em></strong>
										</li>
									</ul>
									<br><br>
									<hr class="wp-block-separator">
									<br><br>
									<ul>
										<li>Template for your reference&nbsp;: <em><strong><a href="https://eu4culture.al/wp-content/uploads/2022/02/01-Grant-Support-Agreement-2.pdf">Grant Support Agreement</a></strong></em><br></li>
										<br>
										<li>Frequently Asked Questions&nbsp;: <em><strong><a href="https://eu4culture.al/wp-content/uploads/2022/10/FAQ_CFP-10-2022_ENG_20221003.pdf">FAQ</a></strong></em><br></li>
										<br>
										<li>Combined Questions and Answers&nbsp;: <em><strong><a href="https://eu4culture.al/wp-content/uploads/2022/10/Combined-QA_ENG-.pdf">Q&A</a></strong></em><br></li>
										<br>
									</ul>
								</div>
								</ul>
							</div>
						</div>
						<div class="cell medium-6 line-height content-margin">
							<h4><strong>Information Sessions </strong> </h4>
							<br>
							<!-- <h5><strong>Info Session (In-person)</strong></h5> -->
							<br>
							<p><strong><u>Date:</u></strong> 18 October 2022</p>
							<p><strong><u>Time:</u></strong> 10.30 AM</p>
							<p><strong><u>Location:</u></strong> Tirana - Rogner Hotel (Stockholm Room)</p>
							<p>Session closed.</p>
							<p>
								<a href="https://eu4culture.al/first-information-session-on-the-2nd-call-for-proposals/"><u><strong>Read more about the session</strong></u></a>
							</p>
							<p>
								<a href="https://eu4culture.al/gallery/18-october-2022-first-information-session-on-the-2nd-call-for-proposals-recraft-the-past-build-up-the-future-2/"><u><strong>Visit the Photo Gallery</strong></u></a>
							</p>
							<br>
							<hr class="wp-block-separator bottom">
							<!-- <h5><strong>Info Session (In-person)</strong> </h5>	 -->
							<br>
							<p><strong><u>Date:</u></strong> 20 October 2022</p>
							<p><strong><u>Time:</u></strong> 11.00 AM</p>
							<p><strong><u>Location:</u></strong> Rubik - Infokulla</p>
							<p>Session closed.</p>
							<p>
								<a href="https://eu4culture.al/20-october-2022-second-information-session-on-the-2nd-call-for-proposalsrecraft-the-past-build-up-the-future-2/"><u><strong>Read more about the session</strong></u></a>
							</p>
							<p>
								<a href="https://eu4culture.al/gallery/20-october-2022-second-information-session-on-the-2nd-call-for-proposals-recraft-the-past-build-up-the-future-2/"><u><strong>Visit the Photo Gallery</strong></u></a>
							</p>
							<br>
							<hr class="wp-block-separator bottom">
							<!-- <h5><strong>Info Session (In-person)</strong></h5> -->
							<br>
							<p><strong><u>Date:</u></strong> 26 October 2022</p>
							<p><strong><u>Time:</u></strong> 11.00 AM</p>
							<p><strong><u>Location:</u></strong> National Youth Congress of Albania - Tirana</p>
							<p>Session closed.</p>
							<p>
								<a href="https://eu4culture.al/26-tetor-2022-seanca-e-trete-informuese-mbi-thirrjen-e-2-te-per-propozimerecraft-the-past-build-up-the-future-2/"><u><strong>Read more about the session</strong></u></a>
							</p>
							<p>
								<a href="https://eu4culture.al/gallery/26-tetor-2022-seanca-e-trete-informuese-mbi-thirrjen-e-2-te-per-propozime-recraft-the-past-build-up-the-future-2/"><u><strong>Visit the Photo Gallery</strong></u></a>
							</p>
						</div>
					</div>
					<hr class="wp-block-separator bottom">
					<div>
						<a href="https://eu4culture.al/wp-content/uploads/2023/02/List-of-applicants-and-projects-recommended-for-funding-2nd-CfP-10-2022.pdf">
							<p>
								<strong>
								This is the list of applicants and projects recommended for funding under the “Re-Craft the Past, Build up the Future 2” Call for Proposals
								</strong>
							</p>
						</a>
					</div>
				<?php } else { ?>
					<div class="grid-x grid-padding-x ">
						<div class="cell medium-6 content-margin wp-container-62054074583ad">
							<p>Aplikanti duhet të dorëzojë formularët e mëposhtëm të aplikimit:</p>
							<div>
								<ul>
									<li>
										<a href="https://eu4culture.al/wp-content/uploads/2022/10/2.-CFP-10-2022-Annex-A-Application-Form.docx"><strong><em><u>Formulari i Aplikimit (Aneksi A)</u></em></strong></a>
									</li>
									<br>
									<li><strong><em><a href="https://eu4culture.al/wp-content/uploads/2022/10/3.-CFP-10-2022-Annex-B-Logframe.docx" target="_blank" rel="noreferrer noopener"><u>Kuadri Logjik (Aneksi B)</u></a></em></strong>
									</li>
									<br>
									<li>
										<strong><em><a href="https://eu4culture.al/wp-content/uploads/2022/10/4.-CFP-10-2022-Annex-C-Application-Budget.xlsx" target="_blank" rel="noreferrer noopener"><u>Buxheti i Aplikimit (Aneksi C)</u></a></em></strong>
									</li>
									<br>
									<li><em><a href="https://eu4culture.al/wp-content/uploads/2022/10/5.-CFP-10-2022-Annex-D-Statement-of-Applicant.docx" target="_blank" rel="noreferrer noopener"><strong><em><u>Deklarata e Aplikantit (Aneksi D)</u></em>&nbsp;</strong></a></em>&nbsp;&nbsp;&nbsp;
									</li>
									<br>
									<li>
										<strong><em><a href="https://eu4culture.al/wp-content/uploads/2022/10/6.-CFP-10-2022-Annex-E-Statement-of-Partner-Organisation.docx" target="_blank" rel="noreferrer noopener"><em><u>Deklarata e Organizatës Partnere (Aneksi E)</u></em></a></em></strong>
									</li>
									<br>
									<li><strong><em><a href="https://eu4culture.al/wp-content/uploads/2022/10/7.-CFP-10-2022-Annex-F-Application-Checklist.docx" target="_blank" rel="noreferrer noopener"><em><u>Lista e kontrollit (Aneksi F)</u></em></a></em></strong>
									</li>
								</ul>
								<br><br>
								<hr class="wp-block-separator">
								<br><br>
								<ul>
									<p>Për referencë : <em><strong><em><em><strong><a href="https://eu4culture.al/wp-content/uploads/2022/02/01-Grant-Support-Agreement-2.pdf"><strong>Marrëveshja tip e Grantit</strong></a></strong></em></em></strong></em><a id="_msocom_1"></a></p>
									<br>
									<p>Pyetje te bera me shpesh: <em><strong><em><em><strong><a href="https://eu4culture.al/wp-content/uploads/2022/10/FAQ_CFP-10-2022_ENG_20221003.pdf"><strong>FAQ</strong></a></strong></em></em></strong></em><a id="_msocom_1"></a></p>
									<br>
									<p>Pyetje dhe Pergjigje: <em><strong><em><em><strong><a href="https://eu4culture.al/wp-content/uploads/2022/10/QA-Pyetje-Pergjigje-te-Kombinuara_ALB.pdf"><strong>Q&A</strong></a></strong></em></em></strong></em><a id="_msocom_1"></a></p>
									<br>
								</ul>
							</div>

						</div>
						<div class="cell medium-6 line-height content-margin">
							<h4><strong>Seanca Informimi</strong> </h4>
							<br>
							<br>
							<p><strong><u>Data:</u></strong> 18 Tetor 2022</p>
							<p><strong><u>Ora:</u></strong> 10.30 </p>
							<p><strong><u>Vëndodhja:</u></strong> Tiranë - Rogner Hotel (Stockholm Room) </p>
							<p>Seancë e mbyllur</p>
							<p>
								<a href="https://eu4culture.al/sq/first-information-session-on-the-2nd-call-for-proposals/"><u><strong>Lexo më shumë për këtë seancë</strong></u></a>
							</p>
							<p>
								<a href="https://eu4culture.al/sq/gallery/18-october-2022-first-information-session-on-the-2nd-call-for-proposals-recraft-the-past-build-up-the-future-2/"><u><strong>Vizitoni Foto Galerinë</strong></u></a>
							</p>
							<br>
							<hr class="wp-block-separator bottom">
							<!-- <h5><strong>Seancë Informuese</strong> </h5> -->
							<br>
							<p><strong><u>Data:</u></strong> 20 Tetor 2022</p>
							<p><strong><u>Ora:</u></strong> 11.00 </p>
							<p><strong><u>Vëndodhja:</u></strong> Rubik - Infokulla</p>
							<p>Seancë e mbyllur</p>
							<p>
								<a href="https://eu4culture.al/sq/20-october-2022-second-information-session-on-the-2nd-call-for-proposalsrecraft-the-past-build-up-the-future-2/"><u><strong>Lexo më shumë për këtë seancë</strong></u></a>
							</p>
							<p>
								<a href="https://eu4culture.al/sq/gallery/20-october-2022-second-information-session-on-the-2nd-call-for-proposals-recraft-the-past-build-up-the-future-2/"><u><strong>Vizitoni Foto Galerinë</strong></u></a>
							</p>
							<br>
							<hr class="wp-block-separator bottom">
							<!-- <h5><strong>Seancë Informuese</strong> </h5>	 -->
							<br>
							<p><strong><u>Data:</u></strong> 26 Tetor 2022</p>
							<p><strong><u>Ora:</u></strong> 11.00</p>
							<p><strong><u>Vëndodhja:</u></strong> Kongresi Rinor Kombetar i Shqipërise - Tirana </p>
							<p>Seancë e mbyllur</p>
							<p>
								<a href="https://eu4culture.al/sq/26-tetor-2022-seanca-e-trete-informuese-mbi-thirrjen-e-2-te-per-propozimerecraft-the-past-build-up-the-future-2/"><u><strong>Lexo më shumë për këtë seancë</strong></u></a>
							</p>
							<p>
								<a href="https://eu4culture.al/sq/gallery/26-tetor-2022-seanca-e-trete-informuese-mbi-thirrjen-e-2-te-per-propozime-recraft-the-past-build-up-the-future-2/"><u><strong>Vizitoni Foto Galerinë</strong></u></a>
							</p>

						</div>
					</div>
					<hr class="wp-block-separator">
					<div>
						<a href="https://eu4culture.al/wp-content/uploads/2023/02/List-of-applicants-and-projects-recommended-for-funding-2nd-CfP-10-2022.pdf">
							<p>
								<strong>
									Kjo është lista e aplikantëve dhe projekteve të rekomanduara për financim në kuadër të Thirrjes për Projekt-Propozime “Re-Craft the Past, Build up the Future 2”
								</strong>
							</p>
						</a>
					</div>
				<?php } ?>

			</div>
		</div>
	</div>
</div>



<?php get_footer(); ?>