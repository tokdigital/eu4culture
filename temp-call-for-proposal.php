<?php /* Template Name: Call For Proposal */ ?>

<?php get_header(); ?>


<img src="<?php bloginfo('template_url') ?>/img/bannerArtboard.jpg" alt="">


    <div class="grid-container">
		<div class="grid-x align-center">
			<div class="cell medium-8">
				<div class="content">
					<?php the_content();?>


						<?php if(ICL_LANGUAGE_CODE=='en'){ ?>
							<div class="grid-x grid-padding-x ">
								<div class="cell medium-6 content-margin">
									<p>The applicant must submit the following application forms:</p>
									<div class="wp-container-62054074583ad wp-block-group">
										<div class="wp-block-group__inner-container">
										<ul>
											<li>
												<mark style="background-color: rgba(0, 0, 0, 0);" class="has-inline-color has-vivid-cyan-blue-color"><em><a href="https://eu4culture.al/wp-content/uploads/2022/02/2.-EU4C_RECRAFT-THE-PAST_CFP-02-2022-Annex-A-Application-Form.docx" target="_blank" rel="noreferrer noopener"><strong><u>Application Form (Annex A)</u></strong></a></em></mark>
											</li>
											<br>
											<li><a href="https://eu4culture.al/wp-content/uploads/2022/02/3.-EU4C_RECRAFT-THE-PAST_CFP-02-2022-Annex-B-Logframe.docx"><em><strong><u>Logical Framework (Annex </u></strong></em><u><em><strong><em><strong>B)</strong></em></strong></em></u></a>
											</li>
											<br>
											<li>
												<mark style="background-color: rgba(0, 0, 0, 0);" class="has-inline-color has-vivid-cyan-blue-color"><em><a href="https://eu4culture.al/wp-content/uploads/2022/02/4.-EU4C_RECRAFT-THE-PAST_CFP-02-2022-Annex-C-Project-Budget.xlsx" target="_blank" rel="noreferrer noopener"><strong><u>Application Budget (Annex C) </u></strong></a></em></mark>
											</li>
											<br>
											<li>
												<em><a href="https://eu4culture.al/wp-content/uploads/2022/02/5.-EU4C_RECRAFT-THE-PAST_CFP-02-2022-Annex-D-Applicant-Statement.docx" target="_blank" rel="noreferrer noopener"><strong><u>Statement of Applicant (Annex D)</u></strong></a></em>
											</li>
											<br>
											<li><strong><em><a href="https://eu4culture.al/wp-content/uploads/2022/02/6.-EU4C_RECRAFT-THE-PAST_CFP-02-2022-Annex-E-Partner-Organisation-Statement.docx" target="_blank" rel="noreferrer noopener"><u>Statement of Partner organization (Annex E)</u></a></em></strong>
											</li>
											<br>
											<li>
												<strong><em><a href="https://eu4culture.al/wp-content/uploads/2022/02/7.-EU4C_RECRAFT-THE-PAST_CFP-02-2022-Annex-F-Application-Check-List.docx" target="_blank" rel="noreferrer noopener"><u>Application Checklist (Annex F)</u></a></em></strong>
											</li>
										</ul>
										<br><br>
										<hr class="wp-block-separator">
										<br><br>
										<ul>
											<li>Template for your reference&nbsp;: <em><strong><a href="https://eu4culture.al/wp-content/uploads/2022/02/01-Grant-Support-Agreement-2.pdf">Grant Support Agreement</a></strong></em><br></li>
												<br>
											<li>Frequently Asked Questions&nbsp;: <em><strong><a href="https://eu4culture.al/wp-content/uploads/2022/10/FAQ_CFP-02-2022_ENG_20220215.docx.pdf">FAQ</a></strong></em><br></li>
											<br>
											<li>Combined Questions and Answers&nbsp;: <em><strong><a href="https://eu4culture.al/wp-content/uploads/2022/02/Combined-QA_RECRAFT-THE-PAST.pdf">Q&A</a></strong></em><br></li>
											<br>
										</ul>
										</div>
									</ul>
									</div>
								</div>
								<div class="cell medium-6 line-height content-margin">
									<h4><strong>Information Sessions </strong> </h4>	
									<br>
									<h5><strong>Info Session (In-person)</strong></h5>
									<br>
									<p><strong><u>Date:</u></strong>  16 February 2022</p>	
									<p><strong><u>Time:</u></strong>  11.00 AM (open from 10:30)</p>	
									<p>Session closed.</p>
									<p>
										 <a href="https://eu4culture.al/information-session-on-1st-call-for-proposals-recraft-the-past-build-up-the-future%ef%bf%bc/"><u><strong>Read more about the session</strong></u></a>
									</p>	
									<p>
										 <a href="https://eu4culture.al/gallery/information-session-on-1st-call-for-proposals-recraft-the-past-build-up-the-future/"><u><strong>Visit the Photo Gallery</strong></u></a>
									</p>	
									<!-- <p>Show your interest to attend by filling in the
										 <a href="https://forms.gle/e8FpyYUpArfa4cGi8"><u><strong>registration form </strong></u></a>
									</p>	 -->
									<!-- <p><i>Note: Make sure to observe the Covid-19 restrictions including physical distancing and use of masks.</i></p> -->
									
									<br>
									<h5><strong>Info Session (Online, if impossible to attend in person)</strong> </h5>	
									<br>
									<p><strong><u>Date:</u></strong> 17 February 2022</p>	
									<p><strong><u>Time:</u></strong> 10.00 AM</p>	
									<p><strong><u>Platform:</u></strong> Google Meet </p>
									<p>Session closed.</p>
									<!-- <p>Show your interest to attend by filling in the
										<a href="https://forms.gle/1pgFGXHk8CexYHdDA"><u><strong>registration form </strong></u></a>
									</p> -->
									<p>
										<a href="https://eu4culture.al/the-2nd-information-session-on-recraft-the-past-build-up-the-future-call/"><u><strong>Read more about the session </strong></u></a>
									</p>
									
								</div>
							</div> 
							<hr class="wp-block-separator bottom">
							<div>
								<a href="https://eu4culture.al/wp-content/uploads/2022/07/List-of-applicants-and-projects-recommended-for-funding-CfP-02-2022.pdf">
									<p>
										<strong>
											This is the list of applicants and projects recommended for funding under the Re-Craft the Past: Build up the Future Call for Proposals
										</strong>
									</p>
								</a>
							</div>
						<?php } else { ?>
							<!-- <a href="https://eu4culture.al/sq/thirrje-per-propozime/" class="button"><?php _e("Open Call For Proposals" , "unops")  ?></a> -->

							<div class="grid-x grid-padding-x ">
								<div class="cell medium-6 content-margin">
									<p>Aplikanti duhet të dorëzojë formularët e mëposhtëm të aplikimit:</p>
									<div>
										<ul>
											<li>
												<a href="https://eu4culture.al/wp-content/uploads/2022/02/2.-EU4C_RECRAFT-THE-PAST_CFP-02-2022-Annex-A-Application-Form.docx"><strong><em><u>Formulari i Aplikimit (Aneksi A)</u></em></strong></a>
											</li>
											<br>
											<li><strong><em><a href="https://eu4culture.al/wp-content/uploads/2022/02/3.-EU4C_RECRAFT-THE-PAST_CFP-02-2022-Annex-B-Logframe.docx" target="_blank" rel="noreferrer noopener"><u>Kuadri Logjik (Aneksi B)</u></a></em></strong>
											</li>
											<br>
											<li>
												<strong><em><a href="https://eu4culture.al/wp-content/uploads/2022/02/4.-EU4C_RECRAFT-THE-PAST_CFP-02-2022-Annex-C-Project-Budget.xlsx" target="_blank" rel="noreferrer noopener"><u>Buxheti i Aplikimit (Aneksi C)</u></a></em></strong>
											</li>
											<br>
											<li><em><a href="https://eu4culture.al/wp-content/uploads/2022/02/5.-EU4C_RECRAFT-THE-PAST_CFP-02-2022-Annex-D-Applicant-Statement.docx" target="_blank" rel="noreferrer noopener"><strong><em><u>Deklarata e Aplikantit (Aneksi D)</u></em>&nbsp;</strong></a></em>&nbsp;&nbsp;&nbsp;
											</li>
											<br>
											<li>
												<strong><em><a href="https://eu4culture.al/wp-content/uploads/2022/02/6.-EU4C_RECRAFT-THE-PAST_CFP-02-2022-Annex-E-Partner-Organisation-Statement.docx" target="_blank" rel="noreferrer noopener"><em><u>Deklarata e Organizatës Partnere (Aneksi E)</u></em></a></em></strong>
											</li>
											<br>
											<li><strong><em><a href="https://eu4culture.al/wp-content/uploads/2022/02/7.-EU4C_RECRAFT-THE-PAST_CFP-02-2022-Annex-F-Application-Check-List.docx" target="_blank" rel="noreferrer noopener"><em><u>Lista e kontrollit (Aneksi F)</u></em></a></em></strong>
											</li>
										</ul>
										<br><br>
										<hr class="wp-block-separator">
										<br><br>
										<ul>
											<p>Për referencë :      <em><strong><em><em><strong><a href="https://eu4culture.al/wp-content/uploads/2022/02/01-Grant-Support-Agreement-2.pdf"><strong>Marrëveshja tip e Grantit</strong></a></strong></em></em></strong></em><a id="_msocom_1"></a></p>
											<br>
											<p>Pyetje te bera me shpesh:      <em><strong><em><em><strong><a href="https://eu4culture.al/wp-content/uploads/2022/10/FAQ_CFP-02-2022_ENG_20220215.docx.pdf"><strong>FAQ</strong></a></strong></em></em></strong></em><a id="_msocom_1"></a></p>
											<br>
											<p>Pyetje dhe Pergjigje:     <em><strong><em><em><strong><a href="https://eu4culture.al/wp-content/uploads/2022/03/Pyetje-Pergjigje-te-Kombinuara_RECRAFT-THE-PAST.pdf"><strong>Q&A</strong></a></strong></em></em></strong></em><a id="_msocom_1"></a></p>
											<br>
										</ul>
									</div>
									
								</div>
								<div class="cell medium-6 line-height content-margin">
									<h4><strong>Seanca Informimi</strong> </h4>	
									<br>
									<h5><strong>Seanca Informimi (pjesëmarrje fizike)</strong></h5>
									<br>
									<p><strong><u>Data:</u></strong>  16 Shkurt 2022</p>	
									<p><strong><u>Ora:</u></strong>  11.00 </p>	
									<p><strong><u>Venue:</u></strong>  Europe House”, Tiranë </p>	
									<p>Seancë e mbyllur</p>
									<p>
										<a href="https://eu4culture.al/sq/seance-informimi-mbi-thirrjen-e-1-re-per-propozime-recraft-the-past-build-up-the-future/"><u><strong>Lexo më shumë për këtë seancë</strong></u></a>
									</p>
									<p>
										<a href="https://eu4culture.al/sq/gallery/seance-informimi-mbi-thirrjen-e-1-re-per-propozime-recraft-the-past-build-up-the-future/"><u><strong>Vizitoni Foto Galerinë</strong></u></a>
									</p>
									<!-- <p>Shprehni interesin tuaj për të marrë pjesë, duke plotësuar 
										 <a href="https://forms.gle/e8FpyYUpArfa4cGi8"><u><strong>formularin e rregjistrimit.</strong></u></a>
									</p>	
									<p><i>Shënim: Sigurohuni të ruani masat e sigurisë ndaj situatës së COVID-19, duke përfshirë dhe ruajtjen e distancave dhe përdorimin e maskave.</i></p>
									 -->
									<br>
									<h5><strong>Seancë Informimi Online</strong> </h5>	
									<br>
									<p><strong><u>Data:</u></strong>  17 Shkurt 2022</p>	
									<p><strong><u>Ora:</u></strong>  10.00 </p>	
									<p><strong><u>Platform:</u></strong>  Google Meet </p>
									<p>Seancë e mbyllur</p>
									<p>
										<a href="https://eu4culture.al/sq/seanca-e-dyte-informuese-mbi-thirrjen-recraft-the-past-build-up-the-future/"><u><strong>Lexo më shumë për këtë seancë </strong></u></a>
									</p>
									<!-- <p>Shprehni interesin tuaj për të marrë pjesë, duke plotësuar 
										 <a href="https://forms.gle/e8FpyYUpArfa4cGi8"><u><strong>formularin e rregjistrimit.</strong></u></a>
									</p>
									<p>Linku i takimit do të dërgohet në adresat e rregjistruara, një ditë përpara eventit.</p> -->
								</div>
							</div> 
							<hr class="wp-block-separator">
							<div>
								<a href="https://eu4culture.al/wp-content/uploads/2022/07/List-of-applicants-and-projects-recommended-for-funding-CfP-02-2022.pdf">
									<p>
										<strong>
											Kjo është lista e aplikantëve dhe projekteve të rekomanduara për financim nën Thirrjen për Projekt-Propozime Re-Craft the Past: Build up the Future 
										</strong>
									</p>
								</a>
							</div>
						<?php } ?> 

				</div>
			</div>
		</div>
	</div>



<?php get_footer(); ?>



