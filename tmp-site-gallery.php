<?php /* Template Name: Site Gallery Page */ ?>

<?php get_header(); ?>

<h1 class="page-title"><?php the_title(); ?></h1>



<section class="section-post-grid">
	<div class="grid-container">
		<div class="grid-x grid-padding-x medium-up-4" data-equalizer data-equalize-on="medium">
			<?php 
			 $args = array(
			 	'post_type' => 'site',
		        'posts_per_page' => -1,
		        );
		    $loop = new WP_Query( $args );
		     ?>
			<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
				<div class="cell">
					<div class="post-item" data-equalizer-watch>
						<a href="<?php the_permalink(); ?>" class="featured-image" target="_blank">
						    <?php the_post_thumbnail(); ?>
						</a>
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						
					</div>
				</div>
			<?php endwhile;endif;wp_reset_postdata();  ?>
		</div>
	</div>
</section>





<?php get_footer(); ?>
