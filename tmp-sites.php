<?php /* Template Name: Sites */ ?>
<?php get_header(); ?>

<article>

	<h1 class="page-title"><?php the_title(); ?></h1>

	<?php get_template_part('mapstructure3') ?>

</article>

<?php get_footer(); ?>