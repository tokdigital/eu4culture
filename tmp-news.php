<?php /* Template Name: News Page */ ?>
<?php get_header(); ?>

<h1 class="page-title"><?php the_title(); ?></h1>

<section class="section-news">
	<div class="grid-container">

		<div class="grid-x grid-padding-x align-center">
			<div class="cell medium-12">
				<div class="news-holder">
					<div class="grid-x grid-padding-x medium-up-4" data-equalizer data-equalize-on="medium">
						<?php
						$args = array(
							'post_type' => 'post',
							'posts_per_page' => -1
						);
						$loop = new WP_Query($args);
						?>
						<?php if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
								<div class="cell">
									<div class="single-news" data-equalizer-watch>
										<a href="<?php the_permalink(); ?>" class="featured-image">
											<?php the_post_thumbnail(); ?>
										</a>
										<a href="<?php the_permalink(); ?>" class="small-title">
											<?php the_title(); ?>
										</a>
										<p style="padding: 0 25px;    min-height: 29px;"><?php $post_date = get_the_date('F j, Y');
																							echo $post_date; ?> </p>
										<a href="<?php the_permalink(); ?>" class="read-more">
											<?php _e("Read More", "unops")  ?> <img src="<?php bloginfo('template_url') ?>/img/arrow-right-red.svg" alt="">
										</a>
										<div class="decor">
											<img src="<?php bloginfo('template_url') ?>/img/news-decor.svg" alt="">
										</div>
									</div>
								</div>
						<?php endwhile;
						endif;
						wp_reset_postdata();  ?>
					</div>
				</div>
			</div>
			<div class="grid-x">
				<div class="button-holder">
					<a href="#" class="default-button"><?php _e("View More", "unops")  ?></a>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>