<?php /* Template Name: About Page */ ?>

<?php get_header(); ?>


<div class="section-about">
	<div class="about-wrapper">
		<div class="grid-container">
			<div class="grid-x grid-padding-x align-center">
				<div class="cell medium-8 text-left">
					<div class="default-title text-center"><?php _e("About the project", "unops")  ?></div>
					<div class="text-holder">
						<p><?php _e("EU4Culture project is funded by the European Union (EU) and is implemented by UNOPS in a close partnership with the Ministry of Culture. It is one of the largest cultural heritage programmes designed by the European Union with a total budget of € 40 million. EU4Culture programme extends to the entire territory affected by the earthquake of November 26, 2019. In addition to the restoration, the project will invest in the revitalization of selected cultural heritage sites, including upgrade of services and site conditions. It’s goal is also to increase tourism potential, directly contributing towards Albania's socio-economic recovery. Local entrepreneurship, artisanship and creative creation evolving around selected sites shall be supported through grants, providing a direct boost to local production and social wellbeing.", "unops")  ?></p>
					</div>
				</div>
				<img src="<?php bloginfo('template_url') ?>/img/artt.svg" alt="" class="show-for-medium art">
			</div>
		</div>
	</div>
	<div class="show-for-medium"><br><br><br><br><br></div>
	<div class="hide-for-medium"><br><br></div>
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-6">
				<div class="img-wrapper">
					<img class="show-for-medium" src="<?php bloginfo('template_url') ?>/img/about-image.jpg" alt="Unops">
				</div>
			</div>
			<div class="cell medium-6">
				<div class="content-wrapper">
					<div class="default-title"><?php _e("Who we are", "unops")  ?></div>
					<div class="text-holder">
						<p><?php _e("UNOPS is a UN agency that provides infrastructure, procurement and project management services for a
						more sustainable world. We combine the best of the UN and the private sector to ensure our partners
						maximize the positive impact of their peace and security, humanitarian and development projects – with
						equality, inclusiveness, sustainability and resilience as the foundations of our work. Unops supports
						projects in more than 80 countries around the world with a total delivery of 2.2 billion $ during 2020.
						We help people build better lives and countries achieve peace and sustainable development. We bring
						to this task the values and principles of the United Nations and the innovation, boldness, speed and
						efficiency of a self-financed institution.", "unops")  ?><br><br>
							<?php _e("UNOPS Albania office was established in September 2020. In Albania, we work with our partners on a
						range of projects. This includes the EU4Culture project, our support to the government of Albania on the
						COVID-19 response efforts, regional (Western Balkans) programs, supporting reform processes to
						achieve EU accession and progress on country development agendas. We also work closely with the UN
						Country team on sustainable development actively contributing to the UN Sustainable Development
						Cooperation Framework 2021-2026 with the Albanian government.", "unops")  ?></p>
					</div>
				</div>
			</div>
			<br><br>

		</div>
	</div>
</div>


<div class="section-numbers">
	<div class="grid-container">
		<div class="grid-x grid-padding-x align-center">
			<div class="cell medium-12">
				<div class="content-wrapper">
					<div class="default-title center"><?php _e("EU4CULTURE In Numbers", "unops")  ?></div>
				</div>
			</div>
			<div class="cell medium-7">
				<div class="text-holder text-center">

				</div>
			</div>
			<div class="cell medium-8">
				<div class="image-holder text-center">
					<img src="<?php bloginfo('template_url') ?>/img/now.jpg" alt="">
				</div>
			</div>
			<div class="cell medium-12">
				<div class="numbers-holder" id="numbers-holder">
					<div class="grid-x grid-padding-x">
						<div class="cell medium-4">
							<a href="https://eu4culture.al/sites/">
								<div class="single-number">
									<div class="icon">
										<img src="<?php bloginfo('template_url') ?>/img/numbers/icon1.svg" alt="">
									</div>
									<div class="content">
										<div class="code1 default-title" data="24">0</div>
										<p><?php _e("Sites targeted by the Programme", "unops")  ?></p>
									</div>
								</div>
							</a>
						</div>



						<div class="cell medium-4">

							<div class="single-number">
								<div class="icon">
									<img src="<?php bloginfo('template_url') ?>/img/numbers/icon2.svg" alt="">
								</div>
								<div class="content">
									<span class="code2 default-title" data="8">0</span>
									<p><?php _e("Municipalities targeted by the Program", "unops")  ?></p>
								</div>
							</div>

						</div>
						<div class="cell medium-4">
							<div class="single-number">
								<div class="icon">
									<img src="<?php bloginfo('template_url') ?>/img/numbers/icon3.svg" alt="">
								</div>
								<div class="content">
									<span class="code3 default-title" data="11">0</span>
									<p><?php _e("Ongoing Sites", "unops")  ?></p>
								</div>
							</div>
						</div>
						<div class="cell medium-4">
							<div class="single-number">
								<div class="icon">
									<img src="<?php bloginfo('template_url') ?>/img/numbers/citiz.png" alt="">
								</div>
								<div class="content">
									<span class="code4 default-title" data="40">0</span>
									<p><?php _e("Total Budget", "unops")  ?></p>
								</div>
							</div>
						</div>


						<div class="cell medium-4">
							<a href="https://eu4culture.al/events/">
								<div class="single-number">
									<div class="icon">
										<img src="<?php bloginfo('template_url') ?>/img/numbers/icon5.svg" alt="">
									</div>
									<div class="content">
										<span class="code5 default-title" data="54">0</span>
										<p><?php _e("Consultation meetings", "unops")  ?></p>
									</div>
								</div>
							</a>
						</div>
						<div class="cell medium-4">
							<div class="single-number">
								<div class="icon">
									<img src="<?php bloginfo('template_url') ?>/img/numbers/icon6.svg" alt="">
								</div>
								<div class="content">
									<span class="code6 default-title" data="250">0</span>
									<p><?php _e("Policymakers, academics, media, community representatives participated in Consultation meetings", "unops")  ?></p>
								</div>
							</div>
						</div>
						<div class="cell medium-4">
							<div class="single-number">
								<div class="icon">
									<img src="<?php bloginfo('template_url') ?>/img/numbers/citiz.png" alt="">
								</div>
								<div class="content">
									<span class="code7 default-title" data="3">0</span>
									<p><?php _e("Restored Sites", "unops")  ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php get_template_part('mapstructure') ?>


<?php get_template_part('file-pdf') ?>


<?php get_footer(); ?>