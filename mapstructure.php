<div class="map-page">
	<div class="grid-container">
		<div class="default-title center"><?php _e("What we do" , "unops")  ?></div>
		<div class="grid-x align-center grid-padding-x">
			<div class="cell">
				<div class="content">
					<div id="map"></div>
				</div>
			</div>
			<div class="cell">
				<div class="locations-table">
					<table>
						<thead>
							<tr>
								<th><?php _e("Site" , "unops")  ?></th>
								<th class="show-for-medium"><?php _e("Category" , "unops")  ?></th>
								<th class="show-for-medium"><?php _e("Region" , "unops")  ?></th>
								<th class="show-for-medium"><?php _e("Administrative Unit" , "unops")  ?></th>
								<th class="show-for-medium"><?php _e("Type of Intervation" , "unops")  ?></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$args2 = array(
								'post_type' => 'location',
								'order' => 'ASC',
								'posts_per_page' => -1 
							);
							$q2 = new WP_query($args2);
							if($q2->have_posts()) :
							    while($q2->have_posts()) : $q2->the_post();
						    		$theID = get_the_ID();
					    	?>
							<tr class="<?php echo $theID; ?>" id="<?php echo $theID; ?>" data-id="<?php echo $theID; ?>">
								<td><?php the_title(); ?></td>
								<td class="show-for-medium"><?php the_field('cultural_heritage_category'); ?></td>
								<td class="show-for-medium"><?php the_field('region'); ?></td>
								<td class="show-for-medium"><?php the_field('administrative_unit'); ?></td>
								<td class="show-for-medium"><?php the_field('type_of_intervention'); ?></td>
								<td><a href="<?php the_permalink(); ?>" class="tiny button"><?php _e("Read More" , "unops")  ?></a></td>
							</tr>
							<?php endwhile;endif; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDcD4eoXwTX32GvWmjeqRVWUDE_uGUVPWk&callback=initMap&libraries=&v=weekly"
      async
    ></script>

<script>
	function initMap() { 
    var centerPosition = {lat: 41.1206216, lng: 19.8708004};
    var map = new google.maps.Map(document.getElementById('map'), {
        center: centerPosition,
        zoom : 8,
        mapTypeControl: false
    });
    var prev_infowindow =false; 

<? 

$locationCounter = 0;
$args2 = array(
	'post_type' => 'location',
	'order' => 'ASC',
	'posts_per_page' => -1 
);
$q2 = new WP_query($args2);
if($q2->have_posts()) :
    while($q2->have_posts()) : $q2->the_post();
	$theID = get_the_ID(); ?>
 	 
	<?php $location = get_field('map_location');  ?>
	var position<?php echo $theID; ?> = {lat: <?php echo esc_attr($location['lat']); ?>, lng: <?php echo esc_attr($location['lng']); ?>};  

    var content<?php echo $theID; ?> = "<h6 class='map-title'><?php  the_title(); ?></h6><p><a class='map-link' href='<?php the_permalink(); ?>'><?php esc_html_e( 'Read more', 'text-domain' ); ?><i class='fa fa-arrow-right' aria-hidden='true'></i></a></p>"; 


    var centerInfo<?php echo $theID; ?> = new google.maps.InfoWindow({
    	content: content<?php echo $theID; ?>
    }); 

    
    var marker<?php echo $theID; ?> = new google.maps.Marker({
	        position: position<?php echo $theID; ?>
	    }); 


    // marker<?php echo $theID; ?>.addListener('click', function() {
    	
    // }); 

    marker<?php echo $theID; ?>.addListener("mouseover", () => {
    	$('.locations-table tr').removeClass('active');
		$('tr.<?php echo $theID; ?>').addClass('active');

		if( prev_infowindow ) {
           prev_infowindow.close();
        }

        prev_infowindow = centerInfo<?php echo $theID; ?>;
        centerInfo<?php echo $theID; ?>.open(map, marker<?php echo $theID; ?>); 
	});

    row<?php echo $theID; ?> = document.getElementById('<?php echo $theID; ?>');
    row<?php echo $theID; ?>.addEventListener("click", () => {
    	$('.locations-table tr').removeClass('active');
		$('tr.<?php echo $theID; ?>').addClass('active');

		map.setZoom(10);
    	map.setCenter(marker<?php echo $theID; ?>.getPosition());

		if( prev_infowindow ) {
           prev_infowindow.close();
        }

        prev_infowindow = centerInfo<?php echo $theID; ?>;
        centerInfo<?php echo $theID; ?>.open(map, marker<?php echo $theID; ?>); 
	});



    marker<?php echo $theID; ?>.setMap(map); 


	<? $locationCounter++;endwhile; endif;?>

}

</script>