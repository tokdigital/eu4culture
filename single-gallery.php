
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>

	<h1 class="page-title"><?php the_title(); ?></h1>
	<div class="single-gallery">
		<div class="grid-container">
			<div class="grid-x">
				<div class="cell">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
		
<?php endwhile;endif; ?>
<?php get_footer(); ?>