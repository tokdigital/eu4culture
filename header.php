<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php the_title(); ?></title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url') ?>/img/fav/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url') ?>/img/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url') ?>/img/fav/favicon-16x16.png">
    <link rel="mask-icon" href="<?php bloginfo('template_url') ?>/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-645CES8Y4K"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'G-645CES8Y4K');
    </script>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


    <div class="canvas-container">
        <div class="off-canvas-inner">
            <div class="off-canvas-menu">
                <button class="close-canvas" aria-label="Close menu">
                    <i class="fas fa-times"></i>
                </button>
                <div class="logo"><a href="<?php echo site_url() ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.svg"></a></div>

                <?php wp_nav_menu(
                    array(
                        'menu' => 'Main Menu'
                    )
                ); ?>
            </div>
        </div>
    </div>

    <!-- <div class="under-construction">
    <h6>Under Construction</h6>
</div> -->

    <button class="show-canvas hide-for-medium"><i class="fas fa-bars"></i></button>


    <div class="canvas-container" style="overflow: visible;">
        <div class="off-canvas-menu">
            <div class="off-canvas-inner">
                <button class="close-canvas hide-for-large" aria-label="Close menu">
                    <span aria-hidden="true">&times;</span>
                </button>
                <!-- <li class="logo"><a href="<?php echo site_url() ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.svg"></a></li> -->
                <ul class="menu main-menu hide-for-large">
                    <?php wp_nav_menu(
                        array(
                            'menu' => 'Main Menu'
                        )
                    ); ?>
                </ul>
            </div>
        </div>
    </div>

    <header class="header">
        <div class="grid-container">
            <div class="grid-x">
                <div class="cell small-10 medium-12">
                    <div class="top-header-2">
                        <img src="<?php bloginfo('template_url') ?>/img/header2-01.jpg" alt="">
                    </div>
                </div>
            </div>
            <div class="grid-x">
                <div class="cell">
                    <div class="bottom-header">
                        <div class="logo">
                            <?php if (ICL_LANGUAGE_CODE == 'en') { ?>
                                <a href="<?php echo site_url(); ?>">
                                    <img src="<?php bloginfo('template_url') ?>/img/logo.svg" alt="">
                                </a>
                            <?php } else { ?>
                                <a href="<?php echo site_url(); ?>/sq">
                                    <img src="<?php bloginfo('template_url') ?>/img/logo.svg" alt="">
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="menu-holder headerMenu show-for-large" id="myHeader"> <!-- ????? -->
                        <?php
                        wp_nav_menu(
                            array(
                                'menu' => 'Main Menu'
                            )
                        );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </header>


    <main class="main-content">