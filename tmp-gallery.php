<?php /* Template Name: Gallery Page */ ?>

<?php get_header(); ?>

<h1 class="page-title"><?php the_title(); ?></h1>

<!-- <section class="section-gallery">
	<div class="grid-container">
		<div class="grid-x align-center">
			<div class="cell medium-10">
				<?php 
				 $args = array(
				 	'post_type' => 'gallery',
			        'posts_per_page' => -1
			        );
			     $loop = new WP_Query( $args );
			     ?>
				<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
				<div class="cell medium-4">
					<div class="single-galleries">
						<a href="<?php the_permalink(); ?>" class="small-title">
							<?php the_title(); ?>
						</a>
						<a href="<?php the_permalink(); ?>" class="read-more">
							See more <img src="<?php  bloginfo('template_url') ?>/img/arrow-right-red.svg" alt="">
						</a>
						<div class="decor">
							<img src="<?php  bloginfo('template_url') ?>/img/news-decor.svg" alt="">
						</div>
					</div>
				</div>
				<?php endwhile;endif;wp_reset_postdata();  ?>
			</div>
		</div>
	</div>
</section> -->


<section class="section-post-grid">
	<div class="grid-container">
		<div class="grid-x grid-padding-x medium-up-4" data-equalizer data-equalize-on="medium">
			<?php 
			 $args = array(
			 	'post_type' => 'gallery',
		        'posts_per_page' => -1,
		        );
		    $loop = new WP_Query( $args );
		     ?>
			<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
				<div class="cell">
					<div class="post-item" data-equalizer-watch>
						<a href="<?php the_permalink(); ?>" class="featured-image" target="_blank">
						    <?php the_post_thumbnail(); ?>
						</a>
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						
					</div>
				</div>
			<?php endwhile;endif;wp_reset_postdata();  ?>

		</div>
	</div>
</section>





<?php get_footer(); ?>
