<div class="map-page wwd">
	<div class="">
		<div class="default-title center"><?php _e("What we do", "unops")  ?></div>
		<div class="grid-x align-center">
			<div class="cell">
				<div class="content">
					<div id="map"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell">
				<div class="locations-table">
					<table>
						<thead class="table-head">
							<tr class="head-wrapper">
								<?php if (ICL_LANGUAGE_CODE == 'en') { ?>
									<th class="head-title"><?php _e("Cultural Heritage Sites", "unops")  ?></th>
									<th class="show-for-medium"><?php _e("Category", "unops")  ?></th>
									<th class="show-for-medium"><?php _e("Municipality", "unops")  ?></th>
									<th class="show-for-medium"><?php _e("Administrative Unit / Village", "unops")  ?></th>
									<th class="show-for-medium"><?php _e("Contract Amount in ALL (VAT excluded)", "unops")  ?></th>
									<th class="show-for-medium"><?php _e("Construction Company", "unops")  ?></th>
									<th class="show-for-medium"><?php _e("Construction Start Date ", "unops")  ?></th>
									<th class="show-for-medium head-title-last"><?php _e("Type of Intervention", "unops")  ?></th>
								<?php } else { ?>
									<th class="head-title"><?php _e("Objektet e Trashëgimisë Kulturore", "unops")  ?></th>
									<th class="show-for-medium"><?php _e("Kategoria", "unops")  ?></th>
									<th class="show-for-medium"><?php _e("Bashkia", "unops")  ?></th>
									<th class="show-for-medium"><?php _e("Njësia Administrative", "unops")  ?></th>
									<th class="show-for-medium"><?php _e("Shuma e kontratës në lekë (pa TVSH)", "unops")  ?></th>
									<th class="show-for-medium"><?php _e("Kompania e ndërtimit", "unops")  ?></th>
									<th class="show-for-medium"><?php _e("Data e fillimit të punimeve ", "unops")  ?></th>
									<th class="show-for-medium head-title-last"><?php _e("Lloji i ndërhyrjes", "unops")  ?></th>
								<?php } ?>
							</tr>
						</thead>
						<tbody class="table-body">
							<?php
							$args2 = array(
								'post_type' => 'location',
								'order' => 'ASC',
								'posts_per_page' => -1
							);
							$q2 = new WP_query($args2);
							if ($q2->have_posts()) :
								while ($q2->have_posts()) : $q2->the_post();
									$theID = get_the_ID();
							?>
									<tr class="<?php echo $theID; ?> clickable-row" id="<?php echo $theID; ?>" data-id="<?php echo $theID; ?>" data-plans="<?php the_field('management_plans'); ?>" data-handover="<?php the_field('date_of_handover'); ?>" data-interpretation="<?php the_field('interpretation'); ?>" data-href="<?php the_permalink(); ?>">
										<td><?php the_title(); ?></td>
										<td class="show-for-medium"><?php the_field('cultural_heritage_category'); ?></td>
										<td class="show-for-medium"><?php the_field('region'); ?></td>
										<td class="show-for-medium"><?php the_field('administrative_unit'); ?></td>
										<td class="show-for-medium"><?php the_field('contract_amount'); ?></td>
										<td class="show-for-medium"><?php the_field('construction_company'); ?></td>
										<td class="show-for-medium"><?php the_field('construction_start_date'); ?></td>
										<td class="show-for-medium"><?php the_field('type_of_intervention'); ?></td>
									</tr>
							<?php endwhile;
							endif; ?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- <?php if (ICL_LANGUAGE_CODE == 'en') { ?>
			<div class="cell medium-3 locations-table stick">
				<div class="cell location-name flex-container"> 
					<div>
						<p>Interpretation/ Digitalisation</p>
					</div>
					<div class="interpretation">Tirana</div>
				</div>
				<div class="flex-container small-boxes">
					<div class="small-box">
						<div class="title"><p>Cultural Heritage Management Plans</p></div>
						<div class="date"><p class="plans">2021</p></div>
					</div>
					<div class="small-box">
						<div class="title"><p>Date of Handover to the Beneficiary </p></div>
						<div class="date"><p class="handover">20.09.2022</p></div>
					</div>
				</div>
			</div>
			<?php } else { ?>
				<div class="cell medium-3 locations-table stick">
				<div class="cell location-name flex-container"> 
					<div>
						<p>Interpretation/ Digitalisation</p>
					</div>
					<div class="interpretation">Tirana</div>
				</div>
				<div class="flex-container small-boxes">
					<div class="small-box">
						<div class="title"><p>Cultural Heritage Management Plans</p></div>
						<div class="date"><p class="plans">2021</p></div>
					</div>
					<div class="small-box">
						<div class="title"><p>Date of Handover to the Beneficiary </p></div>
						<div class="date"><p class="handover">20.09.2022</p></div>
					</div>
				</div>
			</div>
			<?php } ?>  -->
		</div>
	</div>
</div>



<script>
	jQuery(document).ready(function($) {
		$(".clickable-row").click(function() {
			window.location = $(this).data("href");
		});
	});
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDcD4eoXwTX32GvWmjeqRVWUDE_uGUVPWk&callback=initMap&libraries=&v=weekly" async defer>
</script>

<script>
	function initMap() {
		var centerPosition = {
			lat: 41.1206216,
			lng: 19.8708004
		};
		var map = new google.maps.Map(document.getElementById('map'), {
			center: centerPosition,
			zoom: 8,
			mapTypeControl: false
		});
		var prev_infowindow = false;

		<?

		$locationCounter = 0;
		$args2 = array(
			'post_type' => 'location',
			'order' => 'ASC',
			'posts_per_page' => -1
		);
		$q2 = new WP_query($args2);
		if ($q2->have_posts()) :
			while ($q2->have_posts()) : $q2->the_post();
				$theID = get_the_ID(); ?>

				<?php $location = get_field('map_location');  ?>
				var position<?php echo $theID; ?> = {
					lat: <?php echo esc_attr($location['lat']); ?>,
					lng: <?php echo esc_attr($location['lng']); ?>
				};

				var content<?php echo $theID; ?> = "<h6 class='map-title'><?php the_title(); ?></h6><p><a class='map-link' href='<?php the_permalink(); ?>'><?php esc_html_e('Read more', 'text-domain'); ?><i class='fa fa-arrow-right' aria-hidden='true'></i></a></p>";


				var centerInfo<?php echo $theID; ?> = new google.maps.InfoWindow({
					content: content<?php echo $theID; ?>
				});

				var icon = {
					url: "https://eu4culture.al/wp-content/uploads/2022/09/map-marker-icon.png", // url
					scaledSize: new google.maps.Size(50, 50), // scaled size
				};

				var marker<?php echo $theID; ?> = new google.maps.Marker({
					position: position<?php echo $theID; ?>,
					icon: icon,
				});


				// marker<?php echo $theID; ?>.addListener('click', function() {

				// }); 

				marker<?php echo $theID; ?>.addListener("mouseover", () => {
					$('.locations-table tr').removeClass('active');
					$('tr.<?php echo $theID; ?>').addClass('active');

					if (prev_infowindow) {
						prev_infowindow.close();
					}

					prev_infowindow = centerInfo<?php echo $theID; ?>;
					centerInfo<?php echo $theID; ?>.open(map, marker<?php echo $theID; ?>);
				});

				row<?php echo $theID; ?> = document.getElementById('<?php echo $theID; ?>');
				row<?php echo $theID; ?>.addEventListener("click", (e) => {
					$('.locations-table tr').removeClass('active');
					$('tr.<?php echo $theID; ?>').addClass('active');

					var interpretation = document.querySelector('.interpretation');
					var plans = document.querySelector('.plans');
					var handover = document.querySelector('.handover');

					var interpretationText = e.currentTarget.getAttribute('data-interpretation');
					var plansText = e.currentTarget.getAttribute('data-plans');
					var handoverText = e.currentTarget.getAttribute('data-handover');

					interpretation.textContent = interpretationText;
					plans.textContent = plansText;
					handover.textContent = handoverText;

					// console.log('click');

					map.setZoom(10);
					map.setCenter(marker<?php echo $theID; ?>.getPosition());

					if (prev_infowindow) {
						prev_infowindow.close();
					}

					prev_infowindow = centerInfo<?php echo $theID; ?>;
					centerInfo<?php echo $theID; ?>.open(map, marker<?php echo $theID; ?>);
				});



				marker<?php echo $theID; ?>.setMap(map);


		<? $locationCounter++;
			endwhile;
		endif; ?>

	}
</script>