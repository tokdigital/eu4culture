
<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>

<div class="section-press">
	<div class="grid-container">
		<div class="grid-x grid-padding-x align-center ">

			<?php 
			
			$args = array(
			    'posts_per_page' => -1, 
			    'post_type' => 'post',
				'taxonomy' => 'stories',


			);
			$loop = new WP_Query( $args );


			?>
	    	
				<div class="cell medium-8 align-center">
					<div class="content-wrapper">
							<div class="text-holder">
								  <p>
									<?php the_content(); ?>
								</p> 
							</div>
					</div>
				</div>

		</div>
	</div>
</div>

<?php endwhile;endif; ?>
<?php get_footer(); ?>