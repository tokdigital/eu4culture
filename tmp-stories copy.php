<?php /* Template Name: Stories Page */ ?>

<?php get_header(); ?>

<section class="section-stories">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div id="map">
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
 <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCD810uq5JRg7_WemkKg-rB03I55BZOaVY&callback=initMap&libraries=&v=weekly" async>
</script>

	<script>
		function initMap() {
		  const center = { lat: 41.183516, lng: 20.0972018 };
		  const pos1 = { lat: 41.3294667860073, lng: 19.817695946921937 };
		  const pos2 = { lat: 41.32737895025334, lng: 19.817542723302523 };
		  const pos3 = { lat: 41.180460837829145, lng: 19.555650276256113 };
		  const pos4 = { lat: 41.506597056379036, lng: 19.793581711053594 };
		  const pos5 = { lat: 41.30965667102782, lng: 19.446914761292355 };
		  const pos6 = { lat: 41.583308392712965, lng: 19.457903247042978 };
		  const pos7 = { lat: 41.76489641070924, lng: 19.78096013678862};
		  const pos8 = { lat: 41.48634687925821, lng: 19.836487984709244};
		  const pos9 = { lat: 41.76489641070924, lng: 19.78096013678862};
		  const pos10 = { lat: 41.78381398049769, lng: 19.65010259820883};
		  const pos11 = { lat: 41.0468164, lng: 19.4964206};
		  const pos12 = {lat:41.3127417, lng:19.4396822};
		  
		    const map = new google.maps.Map(document.getElementById("map"), {
			    zoom: 8,
			    center: center
			 });
		    
		   
			const contentString2 =
			    '<div id="content">' +
			    '<div id="siteNotice">' +
			    "</div>" +
			    '<h1 id="firstHeading" class="firstHeading">Tirana Region</h1>' +
			    '<div id="bodyContent">' +
			    "Mosaic of the National Historic Museum" +
			    '<br>'+

			   '<a href="https://eu4culture.al/location/mosaic-of-the-national-historic-museum/?preview_id=242&preview_nonce=efde230534&_thumbnail_id=-1&preview=true" target="_blank">Read more</a>'
			    "</div>";
			const infowindow2 = new google.maps.InfoWindow({
			    content: contentString2,
			});
			const contentString3 =
			    '<div id="content">' +
			    '<div id="siteNotice">' +
			    "</div>" +
			    '<h1 id="firstHeading" class="firstHeading">Tirana Region</h1>' +
			    '<div id="bodyContent">' +
			    "National Puppet Theatre" +
			    '<br>'+

			   '<a href="https://eu4culture.al/location/national-puppet-theatre/" target="_blank">Read more</a>'
			    "</div>";
			const infowindow3 = new google.maps.InfoWindow({
			    content: contentString3,
			});
			const contentString4 =
			    '<div id="content">' +
			    '<div id="siteNotice">' +
			    "</div>" +
			    '<h1 id="firstHeading" class="firstHeading">Kavaja Region</h1>' +
			    '<div id="bodyContent">' +
			    "Kavaja Ethnographic Museum" +
			    '<br>'+

			   '<a href="https://eu4culture.al/location/kavaja-ethnographic-museum/" target="_blank">Read more</a>'
			    "</div>";
			const infowindow4 = new google.maps.InfoWindow({
			    content: contentString4,
			});
			const contentString5 =
			    '<div id="content">' +
			    '<div id="siteNotice">' +
			    "</div>" +
			    '<h1 id="firstHeading" class="firstHeading">Durres Region</h1>' +
			    '<div id="bodyContent">' +
			    "Kruja Ethnographic Museum" +
			    '<br>'+

			   '<a href="https://eu4culture.al/location/kruja-ethnographic-museum/" taget="_blank">Read more</a>'
			    "</div>";
			const infowindow5 = new google.maps.InfoWindow({
			    content: contentString5,
			});
			const contentString6 =
			    '<div id="content">' +
			    '<div id="siteNotice">' +
			    "</div>" +
			    '<h1 id="firstHeading" class="firstHeading">Durres Region</h1>' +
			    '<div id="bodyContent">' +
			    "Venetian Tower" +
			    '<br>'+

			   '<a href="https://eu4culture.al/location/venetian-tower/" target="_blank">Read more</a>'
			    "</div>";
			const infowindow6 = new google.maps.InfoWindow({
			    content: contentString6,
			});
		
			const contentString7 =
			    '<div id="content">' +
			    '<div id="siteNotice">' +
			    "</div>" +
			    '<h1 id="firstHeading" class="firstHeading">Durres Region</h1>' +
			    '<div id="bodyContent">' +
			    "St. Anthony Church in Rodoni" +
			    '<br>'+

			   '<a href="https://eu4culture.al/location/st-anthony-church-in-rodoni/" target="_blank">Read more</a>'
			    "</div>";
			const infowindow7 = new google.maps.InfoWindow({
			    content: contentString7,
			});
				const contentString8 =
			    '<div id="content">' +
			    '<div id="siteNotice">' +
			    "</div>" +
			    '<h1 id="firstHeading" class="firstHeading">Durres Region</h1>' +
			    '<div id="bodyContent">' +
			    "Kurcaj Bridge" +
			    '<br>'+

			   '<a href="https://eu4culture.al/location/kurcaj-bridge/" target="_blank">Read more</a>'
			    "</div>";
			const infowindow8 = new google.maps.InfoWindow({
			    content: contentString8,
			});
			
			const contentString9 =
			    '<div id="content">' +
			    '<div id="siteNotice">' +
			    "</div>" +
			    '<h1 id="firstHeading" class="firstHeading">Lezha Region</h1>' +
			    '<div id="bodyContent">' +
			    "Monastery Church of Rubik" +
			    '<br>'+

			   '<a href="https://eu4culture.al/location/monastery-church-of-rubik/" target="_blank">Read more</a>'
			    "</div>";
			const infowindow9 = new google.maps.InfoWindow({
			    content: contentString9,
			});
			const contentString10 =
		    '<div id="content">' +
		    '<div id="siteNotice">' +
		    "</div>" +
		    '<h1 id="firstHeading" class="firstHeading">Lezha Region</h1>' +
		    '<div id="bodyContent">' +
		    "Lezha Castle" +
		    '<br>'+

		   '<a href="https://eu4culture.al/location/lezha-castle/" target="_blank">Read more</a>'
		    "</div>";
			const infowindow10 = new google.maps.InfoWindow({
			    content: contentString10,
			});
			const contentString11 =
		    '<div id="content">' +
		    '<div id="siteNotice">' +
		    "</div>" +
		    '<h1 id="firstHeading" class="firstHeading">Kavaja Region</h1>' +
		    '<div id="bodyContent">' +
		    "Castle of Bashtova" +
		    '<br>'+

		   '<a href="https://eu4culture.al/location/castle-of-bashtova/" target="_blank">Read more</a>'
		    "</div>";
			const infowindow11 = new google.maps.InfoWindow({
			    content: contentString11,
			});
			const contentString12 =
		    '<div id="content">' +
		    '<div id="siteNotice">' +
		    "</div>" +
		    '<h1 id="firstHeading" class="firstHeading">Durres Region</h1>' +
		    '<div id="bodyContent">' +
		    "Archaeological Museum In Durres" +
		    '<br>'+

		   '<a href="https://eu4culture.al/location/archaeological-museum-in-durres-2/" target="_blank">Read more</a>'
		    "</div>";
			const infowindow12 = new google.maps.InfoWindow({
			    content: contentString12,
			});





		
	 	const marker2 = new google.maps.Marker({
	    position: pos1,
	    map,
	    title: "Mosaic of the National Historic Museum"
	  		});
		const marker3 = new google.maps.Marker({
		    position: pos2,
		    map,
		    title: "National Puppet Theatre"
		  });
		const marker4 = new google.maps.Marker({
		    position: pos3,
		    map,
		    title: "Kavaja Ethnographic Museum"
		  });
		const marker5 = new google.maps.Marker({
		    position: pos4,
		    map,
		    title: "Kruja Ethnographic Museum"
		  });
		const marker6 = new google.maps.Marker({
		    position: pos5,
		    map,
		    title: "Venetian Tower"
		  });
		const marker7 = new google.maps.Marker({
		    position: pos6,
		    map,
		    title: "St. Anthony Church in Rodoni"
		  });
		const marker8 = new google.maps.Marker({
		    position: pos8,
		    map,
		    title: "Kurcaj Bridge"
		  });
		const marker9 = new google.maps.Marker({
		    position: pos7,
		    map,
		    title: "Monastery Church of Rubik"
		  });
		const marker10 = new google.maps.Marker({
		    position: pos10,
		    map,
		    title: "Lezha Castle"
		  });
		const marker11 = new google.maps.Marker({
		    position: pos11,
		    map,
		    title: "Castle of Bashtova"
		  });
		const marker12 = new google.maps.Marker({
		    position: pos12,
		    map,
		    title: "Archaeological Museum In Durres"
		  });



		
		marker2.addListener("click", () => {
		    infowindow2.open({
		    anchor: marker2,
		    map,
		    shouldFocus: false,
		    });
		  });
		marker3.addListener("click", () => {
		    infowindow3.open({
		    anchor: marker3,
		    map,
		    shouldFocus: false,
		    });
		  });
		marker4.addListener("click", () => {
		    infowindow4.open({
		    anchor: marker4,
		    map,
		    shouldFocus: false,
		    });
		  });
		marker5.addListener("click", () => {
		    infowindow5.open({
		    anchor: marker5,
		    map,
		    shouldFocus: false,
		    });
		  });
		marker6.addListener("click", () => {
		    infowindow6.open({
		    anchor: marker6,
		    map,
		    shouldFocus: false,
		    });
		  });
		marker7.addListener("click", () => {
		    infowindow7.open({
		    anchor: marker7,
		    map,
		    shouldFocus: false,
		    });
		  });
		marker8.addListener("click", () => {
		    infowindow8.open({
		    anchor: marker8,
		    map,
		    shouldFocus: false,
		    });
		  });
		marker9.addListener("click", () => {
		    infowindow9.open({
		    anchor: marker9,
		    map,
		    shouldFocus: false,
		    });
		  });
		marker10.addListener("click", () => {
		    infowindow10.open({
		    anchor: marker10,
		    map,
		    shouldFocus: false,
		    });
		  });
		marker11.addListener("click", () => {
		    infowindow11.open({
		    anchor: marker11,
		    map,
		    shouldFocus: false,
		    });
		  });
		marker12.addListener("click", () => {
		    infowindow12.open({
		    anchor: marker12,
		    map,
		    shouldFocus: false,
		    });
		  });


		}



	




	</script> -->